sleep 10
while true; do
    flask db upgrade
    if [[ "$?" == "0" ]]; then
        break
    fi
    echo Upgrade command failed, retrying in 5 secs...
    sleep 5
done

# python initDB.py
python updateDB.py
gunicorn -b :5000 app:app
