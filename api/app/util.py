import re

def get_info_for_game(game_id):
    game_id = format_game_id(game_id)
    first_year_ending = game_id[3:5]
    season_type = 'Regular Season' if game_id[2] == '2' else 'Playoffs'
    if int(first_year_ending) > 90:
        season = format_year_string('19' + first_year_ending)
    else:
        season = format_year_string('20' + first_year_ending)

    return {
        'season': str(season),
        'season_type': str(season_type)
    }

def format_game_id(game_id):
    game_id = str(game_id)
    while len(game_id) < 10:
        game_id = '0{}'.format(game_id)
    return game_id

def format_year_string(year):
    year = str(year)
    if re.match(r'[1-2][09][0-9]{2}-[0-9]{2}', year):
        return year
    elif re.match(r'[1-2][09][0-9]{2}', year):
        return str(year) + "-" + str(int(year) + 1)[2:4]
    else:
        raise ValueError('Unexpected Year Format!')