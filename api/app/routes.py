import re
import json
from functools import reduce
import operator

from flask import jsonify, request
from sqlalchemy import or_
from util import get_info_for_game

from app import app
from app.models import Game, Rotation, ScoreMargin, Shot, PlayerGameStats, Team, PlayTypeStats, Player

play_type_season_type_map = {
    'Regular Season': 'Reg',
    'Playoffs': 'Post'
}


@app.route('/api/games')
def get_games():
    return jsonify([g.serialize for g in Game.query.order_by(Game.date).all()])


@app.route('/api/gameDetail/<game_id>')
def get_game(game_id):
    data = {
        'summary': Game.query.filter_by(id=game_id).first().serialize,
        'rotations': [r.serialize for r in Rotation.query.filter_by(game_id=game_id)],
        'score': [s.serialize for s in ScoreMargin.query.filter_by(game_id=game_id)],
        'shots': [s.serialize for s in Shot.query.filter_by(game_id=game_id)],
        'stats': [s.serialize for s in PlayerGameStats.query.filter_by(game_id=game_id)]
    }
    return jsonify(data)


@app.route('/api/teams')
def get_teams():
    return jsonify([team.serialize for team in Team.query.order_by(Team.name).all()])


@app.route('/api/teamDetail', methods=['POST'])
def get_team_detail():
    post = request.get_json()
    team = post.get('team')
    season = post.get('season')
    season_type = post.get('seasonType')

    games = Game.query.filter_by(season=season, season_type=season_type).filter(
        or_(Game.home_team == team, Game.away_team == team))

    game_ids = [g.serialize['id'] for g in games]

    rotations = Rotation.query.filter_by(
        team=team).filter(Rotation.game_id.in_(game_ids))

    shots = Shot.query.filter_by(team=team).filter(Shot.game_id.in_(game_ids))
    opp_shots = Shot.query.filter(
        Shot.game_id.in_(game_ids), Shot.team != team)

    play_type_stats = PlayTypeStats.query.filter_by(season=post.get(
        'season'), seasonType=play_type_season_type_map[post.get('seasonType')], isTeam=True)

    player_stats = get_team_player_stats(team, game_ids)

    return jsonify({
        'rotations': [r.serialize for r in rotations],
        'shots': [s.serialize for s in shots],
        'oppShots': [s.serialize for s in opp_shots],
        'playType': [pt.serialize for pt in play_type_stats],
        'stats': player_stats
    })


@app.route('/api/players')
def get_players():
    return jsonify([p.serialize for p in Player.query.order_by(Player.name).all()])


@app.route('/api/playerDetail', methods=['POST'])
def get_player():
    post = request.get_json()

    player = post.get('player')
    season = post.get('season')
    season_type = post.get('seasonType')

    games = Game.query.filter_by(season=season, season_type=season_type)
    game_ids = [g.serialize['id'] for g in games]

    shots = Shot.query.filter_by(shooter=player).filter(
        Shot.game_id.in_(game_ids))

    assists = Shot.query.filter_by(assister=player).filter(
        Shot.game_id.in_(game_ids))

    play_type_stats = PlayTypeStats.query.filter_by(season=post.get(
        'season'), seasonType=play_type_season_type_map[post.get('seasonType')], isTeam=False)

    stats = [gameStats.serialize for gameStats in PlayerGameStats.query.filter_by(
        player=player)]
    unique_info = [json.loads(y) for y in list(
        set(map(lambda x: json.dumps(get_info_for_game(x['game_id'])), stats)))]

    stats_data = []
    for x in unique_info:
        filtered_games = filter(
            lambda y:
            (y['game_info']['season'] == str(x['season'])) &
                (y['game_info']['season_type'] == str(x['season_type'])) &
                (y['time'] > 1),
            stats)

        season_stats = get_player_total_stats(filtered_games)

        if season_stats != -1:
            season_stats['season'] = x['season']
            season_stats['seasonType'] = x['season_type']
            stats_data.append(season_stats)

    shot_data = [shot.serialize for shot in shots]
    shot_data.extend([assist.serialize for assist in assists])

    return jsonify({
        'shots': shot_data,
        'playType': [play_type_stat.serialize for play_type_stat in play_type_stats],
        'stats': stats_data
    })


@app.route('/api/players/consistency', methods=['POST'])
def get_player_consistency_stats():
    post = request.get_json()
    season = post.get('season')
    season_type = post.get('seasonType')
    num_players = post.get('numPlayers')
    team = post.get('team')
    stat = post.get('stat').split('.')

    game_objects = Game.query.filter_by(season=season, season_type=season_type)
    game_ids = [g.serialize['id'] for g in game_objects]

    game_stat_objects = PlayerGameStats.query.filter(
        PlayerGameStats.game_id.in_(game_ids), PlayerGameStats.time >= 1)

    if team != '':
        game_stat_objects = game_stat_objects.filter(PlayerGameStats.team == team)

    stats_log = [stats.serialize for stats in game_stat_objects]
    players = list(set([x['player']['name'] for x in stats_log]))

    total_stats = []

    for player in players:
        player_stats = [stats for stats in stats_log if stats['player']['name'] == player]
        player_total_stats = get_player_total_stats(player_stats)
        player_stat_list = [get_stat(game, stat) for game in player_stats]
        player_total_stats['list'] = player_stat_list

        if player_total_stats != -1:
            total_stats.append(player_total_stats)

    total_stats = sorted(total_stats, key=lambda x: get_stat(x, stat), reverse=True)[:num_players]
    return jsonify(total_stats)


def get_team_player_stats(team, game_ids):
    game_stats = PlayerGameStats.query.filter_by(
        team=team).filter(PlayerGameStats.game_id.in_(game_ids), PlayerGameStats.time >= 1)
    game_stats_serialized = [x.serialize for x in game_stats]
    players = list(set([x['player']['name'] for x in game_stats_serialized]))

    total_stats = []

    for player in players:
        player_stats = [
            x for x in game_stats_serialized if x['player']['name'] == player]

        player_total_stats = get_player_total_stats(player_stats)

        if player_total_stats != -1:
            total_stats.append(player_total_stats)

    return total_stats


def get_player_total_stats(player_stats):
    if len(player_stats) == 0:
        return -1

    teams = list(set(map(lambda x: x['player']['team'], player_stats)))

    player_total_stats = {
        'games_played': len(player_stats),
        'player': {
            'name': player_stats[0]['player']['name'],
            'team': teams,
            'starter': len([x for x in player_stats if x['player']['starter']])
        },
        'team': {
            'pts': sum([x['team']['pts'] for x in player_stats]),
            'opp_pts': sum([x['team']['opp_pts'] for x in player_stats])
        },
        'time': sum([x['time'] for x in player_stats]),
        'scoring': {
            'fg2a': sum([x['scoring']['fg2a'] for x in player_stats]),
            'fg2m': sum([x['scoring']['fg2m'] for x in player_stats]),
            'fg3a': sum([x['scoring']['fg3a'] for x in player_stats]),
            'fg3m': sum([x['scoring']['fg3m'] for x in player_stats]),
            'ft1a': sum([x['scoring']['ft1a'] for x in player_stats]),
            'ft2a': sum([x['scoring']['ft2a'] for x in player_stats]),
            'ft3a': sum([x['scoring']['fg3a'] for x in player_stats]),
            'tech_fta': sum([x['scoring']['tech_fta'] for x in player_stats]),
            'ftm': sum([x['scoring']['ftm'] for x in player_stats]),
            'pts': sum([x['scoring']['pts'] for x in player_stats]),
            'tsa': sum([x['scoring']['tsa'] for x in player_stats]),
        },
        'passing': {
            'passes': sum([x['passing']['passes'] for x in player_stats]),
            'ast': sum([x['passing']['ast'] for x in player_stats]),
            'potential_ast': sum([x['passing']['potential_ast'] for x in player_stats]),
            'ft_ast': sum([x['passing']['ft_ast'] for x in player_stats]),
            'secondary_ast': sum([x['passing']['secondary_ast'] for x in player_stats]),
            'ast_pts': sum([x['passing']['ast_pts'] for x in player_stats])
        },
        'tov': sum([x['tov'] for x in player_stats]),
        'rebounding': {
            'reb': sum([x['rebounding']['reb'] for x in player_stats]),
            'reb_chances': sum([x['rebounding']['reb_chances'] for x in player_stats]),
            'oreb': sum([x['rebounding']['oreb'] for x in player_stats]),
            'oreb_chances': sum([x['rebounding']['oreb_chances'] for x in player_stats]),
            'dreb': sum([x['rebounding']['dreb'] for x in player_stats]),
            'dreb_chances': sum([x['rebounding']['dreb_chances'] for x in player_stats]),
            'box_outs': sum([x['rebounding']['box_outs'] for x in player_stats]),
        },
        'defense': {
            'rim_protection': {
                'dfga': sum([x['defense']['rim_protection']['dfga'] for x in player_stats]),
                'dfgm': sum([x['defense']['rim_protection']['dfgm'] for x in player_stats])
            },
            'shot_contest': {
                'fg2': sum([x['defense']['shot_contest']['fg2'] for x in player_stats]),
                'fg3': sum([x['defense']['shot_contest']['fg3'] for x in player_stats])
            },
            'blk': sum([x['defense']['blk'] for x in player_stats]),
            'stl': sum([x['defense']['stl'] for x in player_stats]),
            'deflections': sum([x['defense']['deflections'] for x in player_stats]),
            'recoveries': sum([x['defense']['recoveries'] for x in player_stats]),
            'charges_drawn': sum([x['defense']['charges_drawn'] for x in player_stats])
        },
        'possession': {
            'time_of_poss': sum([x['possession']['time_of_poss'] for x in player_stats]),
            'touches': sum([x['possession']['touches'] for x in player_stats]),
            'elbow': sum([x['possession']['elbow'] for x in player_stats]),
            'paint': sum([x['possession']['paint'] for x in player_stats]),
            'post': sum([x['possession']['post'] for x in player_stats]),
        },
        'movement': {
            'distance': sum([x['movement']['distance'] for x in player_stats])
        },
        'plus_minus': {
            'raw': sum([x['plus_minus']['raw'] for x in player_stats])
        },
        'screen_ast': sum([x['screen_ast'] for x in player_stats]),
        'possessions': sum([x['possessions'] for x in player_stats])
    }
    player_total_stats['scoring']['ts_pct'] = float(player_total_stats['scoring']['pts']) / (
        player_total_stats['scoring']['tsa'] * 2) if player_total_stats['scoring']['tsa'] > 0 else 0
    player_total_stats['movement']['speed'] = float(
        player_total_stats['movement']['distance']) / player_total_stats['time'] if player_total_stats['time'] > 0 else 0
    player_total_stats['plus_minus']['ortg'] = float(player_total_stats['team']['pts'] / (
        player_total_stats['possessions'])) * 100 if player_total_stats['possessions'] > 0 else 0
    player_total_stats['plus_minus']['drtg'] = float(player_total_stats['team']['opp_pts'] / (
        player_total_stats['possessions'])) * 100 if player_total_stats['possessions'] > 0 else 0
    player_total_stats['pace'] = float(player_total_stats['possessions']) / (
        player_total_stats['time'] / (60 * 48)) if (player_total_stats['time'] / (60 * 48)) > 0 else 0

    return player_total_stats


def get_stat(object, stat):
    return reduce(operator.getitem, stat, object)