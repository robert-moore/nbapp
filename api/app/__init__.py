from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS

import os

from app.config import DevConfig, DockerConfig

app = Flask(__name__)
app.config.from_object(DockerConfig if os.environ.get('CONFIG') == 'docker' else DevConfig)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
CORS(app)

from app import routes, models
