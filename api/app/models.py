from app import db
from util import get_info_for_game


class Game(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    home_team = db.Column(db.String(3))
    away_team = db.Column(db.String(3))
    home_score = db.Column(db.Integer)
    away_score = db.Column(db.Integer)
    season = db.Column(db.Integer)
    season_type = db.Column(db.String)
    date = db.Column(db.Date)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'date': self.date,
            'season': self.season,
            'season_type': self.season_type,
            'home': {'team': self.home_team, 'score': self.home_score},
            'away': {'team': self.away_team, 'score': self.away_score}
        }


class Team(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name
        }


class Rotation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    game_id = db.Column(db.Integer)
    player = db.Column(db.String(50))
    team = db.Column(db.String(3))
    start_time = db.Column(db.Integer)
    end_time = db.Column(db.Integer)

    @property
    def serialize(self):
        return {
            'game_id': self.game_id,
            'player': self.player,
            'team': self.team,
            'start': self.start_time,
            'end': self.end_time        
        }


class ScoreMargin(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    game_id = db.Column(db.Integer)
    minute = db.Column(db.Float)
    margin = db.Column(db.Integer)

    @property
    def serialize(self):
        return {
            'game_id': self.game_id,
            'minute': self.minute,
            'margin': self.margin
        }


class Shot(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    game_id = db.Column(db.Integer)
    shooter = db.Column(db.String(50))
    assister = db.Column(db.String(50))
    team = db.Column(db.String(3))
    x = db.Column(db.Integer)
    y = db.Column(db.Integer)
    made = db.Column(db.Boolean)
    value = db.Column(db.Integer)
    zone = db.Column(db.String(20))
    side = db.Column(db.String(2))

    @property
    def serialize(self):
        return {
            'game_id': self.game_id,
            'shooter': self.shooter,
            'assister': self.assister,
            'team': self.team,
            'x': self.x,
            'y': self.y,
            'made': self.made,
            'value': self.value,
            'zone': self.zone,
            'side': self.side
        }


class Player(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name
        }


class PlayerGameStats(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    game_id = db.Column(db.Integer)
    player = db.Column(db.String(50))
    team = db.Column(db.String(3))
    starter = db.Column(db.Boolean)
    time = db.Column(db.Integer)
    fg2a = db.Column(db.Integer)
    fg2m = db.Column(db.Integer)
    fg3a = db.Column(db.Integer)
    fg3m = db.Column(db.Integer)
    ft1a = db.Column(db.Integer)
    ft2a = db.Column(db.Integer)
    ft3a = db.Column(db.Integer)
    tech_fta = db.Column(db.Integer)
    ftm = db.Column(db.Integer)
    ast = db.Column(db.Integer)
    pot_ast = db.Column(db.Integer)
    ft_ast = db.Column(db.Integer)
    sec_ast = db.Column(db.Integer)
    passes = db.Column(db.Integer)
    ast_pts = db.Column(db.Integer)
    tov = db.Column(db.Integer)
    oreb = db.Column(db.Integer)
    dreb = db.Column(db.Integer)
    orebc = db.Column(db.Integer)
    drebc = db.Column(db.Integer)
    box_outs = db.Column(db.Integer)
    blk = db.Column(db.Integer)
    dfga_rim = db.Column(db.Integer)
    dfgm_rim = db.Column(db.Integer)
    stl = db.Column(db.Integer)
    defl = db.Column(db.Integer)
    recoveries = db.Column(db.Integer)
    charges_drawn = db.Column(db.Integer)
    cont_fg2a = db.Column(db.Integer)
    cont_fg3a = db.Column(db.Integer)
    screen_ast = db.Column(db.Integer)
    poss_time = db.Column(db.Integer)
    touch = db.Column(db.Integer)
    elbow_touch = db.Column(db.Integer)
    post_touch = db.Column(db.Integer)
    paint_touch = db.Column(db.Integer)
    speed = db.Column(db.Float)
    distance = db.Column(db.Integer)
    plus_minus = db.Column(db.Integer)
    ortg = db.Column(db.Float)
    drtg = db.Column(db.Float)
    pace = db.Column(db.Float)

    @property
    def serialize(self):
        pts = (self.fg2m * 2) + (self.fg3m * 3) + self.ftm
        tsa = self.fg2a + self.fg3a + self.ft1a + (self.ft2a / 2) + (self.ft3a / 3)
        poss = ((float(self.time) / 60) / 48) * self.pace
        return {
            'game_id': self.game_id,
            'game_info': get_info_for_game(self.game_id),
            'player': {
                'name': self.player,
                'team': self.team,
                'starter': self.starter
            },
            'team': {
                'pts': self.ortg * (poss / 100) if poss != 0 else 0,
                'opp_pts': self.drtg * (poss / 100) if poss != 0 else 0
            },
            'time': self.time,
            'scoring': {
                'fg2a': self.fg2a,
                'fg2m': self.fg2m,
                'fg3a': self.fg3a,
                'fg3m': self.fg3m,
                'ft1a': self.ft1a,
                'ft2a': self.ft2a,
                'ft3a': self.ft3a,
                'tech_fta': self.tech_fta,
                'ftm': self.ftm,
                'pts': pts,
                'tsa': tsa
            },
            'passing': {
                'passes': self.passes,
                'ast': self.ast,
                'potential_ast': self.pot_ast,
                'ft_ast': self.ft_ast,
                'secondary_ast': self.sec_ast,
                'ast_pts': self.ast_pts,
            },
            'tov': self.tov,
            'rebounding': {
                'reb': (self.oreb + self.dreb),
                'reb_chances': (self.orebc + self.drebc),
                'oreb': self.oreb,
                'oreb_chances': self.orebc,
                'dreb': self.dreb,
                'dreb_chances': self.drebc,
                'box_outs': self.box_outs
            },
            'defense': {
                'rim_protection': {
                    'dfga': self.dfga_rim,
                    'dfgm': self.dfgm_rim
                },
                'shot_contest': {
                    'fg2': self.cont_fg2a,
                    'fg3': self.cont_fg3a
                },
                'blk': self.blk,
                'stl': self.stl,
                'deflections': self.defl,
                'recoveries': self.recoveries,
                'charges_drawn': self.charges_drawn
            },
            'possession': {
                'time_of_poss': self.poss_time,
                'touches': self.touch,
                'elbow': self.elbow_touch,
                'paint': self.paint_touch,
                'post': self.post_touch
            },
            'movement': {
                'distance': self.distance,
                'speed': self.speed
            },
            'plus_minus': {
                'raw': self.plus_minus,
                'ortg': self.ortg,
                'drtg': self.drtg
            },
            'screen_ast': self.screen_ast,
            'pace': self.pace,
            'possessions': poss
        }


class PlayTypeStats(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    poss = db.Column(db.Integer)
    possG = db.Column(db.Float)
    freq = db.Column(db.Float)
    ppp = db.Column(db.Float)
    playType = db.Column(db.String(20))
    season = db.Column(db.Integer)
    seasonType = db.Column(db.String(4))
    isTeam = db.Column(db.Boolean)

    @property
    def serialize(self):
        return {
            'type': self.playType,
            'name': self.name,
            'freq': self.freq,
            'ppp': self.ppp,
            'poss': self.poss,
            'possG': self.possG,
            'isTeam': self.isTeam
        }
