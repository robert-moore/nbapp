import pandas as pd

df = pd.read_csv('shots.csv')

zone_sides = df[['zone', 'side']].drop_duplicates()
zone_info = {}

for i, zs in zone_sides.iterrows():
    if zs.zone not in zone_info:
        zone_info[zs.zone] = {}
    
    zone_df = df[(df['zone'] == zs.zone) & (df['side'] == zs.side)]

    reduced_df = zone_df[['x', 'y']]
    reduced_df.loc['x'] = reduced_df['x'] / 10
    reduced_df.loc['y'] = reduced_df['x'] / 10

    reduced_df = zone_df[['x', 'y']].drop_duplicates()

    reduced_df = reduced_df.sort_values(by=['x', 'y'], ascending=False)
    print(reduced_df)

    zone_info[zs.zone][zs.side] = {
        'pct_take': float(len(zone_df)) / len(df),
        'pct_make': float(len(zone_df[zone_df['made']])) / len(zone_df),
        'size': len(reduced_df)
    }

print(zone_info)
