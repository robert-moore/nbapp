from app import db
import pandas as pd
import logging
import sys

logger = logging.getLogger()
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

logger.info('Update DB')

df = pd.read_csv('data/games.csv')
df.to_sql('game', db.engine, if_exists='append', index=False, chunksize=1000)

df = pd.read_csv('data/rotations.csv')
df.to_sql('rotation', db.engine, if_exists='append', index=False, chunksize=1000)

df = pd.read_csv('data/score.csv')
df.to_sql('score_margin', db.engine, if_exists='append', index=False, chunksize=1000)

df = pd.read_csv('data/shots.csv')
df.to_sql('shot', db.engine, if_exists='append', index=False, chunksize=1000)

df = pd.read_csv('data/stats.csv')
df.to_sql('player_game_stats', db.engine, if_exists='append', index=False, chunksize=1000)

# df = pd.read_csv('data/playType.csv')
# df.to_sql('play_type_stats', db.engine, if_exists='append', index=False)
