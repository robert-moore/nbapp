from app import db
import pandas as pd
import logging
import sys

logger = logging.getLogger()
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

logger.info('Init DB')

# df = pd.read_csv('historical-data/games.csv')
# df.to_sql('game', db.engine, if_exists='append', index=False, chunksize=1000)

# df = pd.read_csv('historical-data/rotations.csv')
# df.to_sql('rotation', db.engine, if_exists='append', index=False, chunksize=1000)

# df = pd.read_csv('historical-data/score.csv')
# df.to_sql('score_margin', db.engine, if_exists='append', index=False, chunksize=1000)

# df = pd.read_csv('historical-data/shots.csv')
# df.to_sql('shot', db.engine, if_exists='append', index=False, chunksize=1000)

# df = pd.read_csv('historical-data/stats.csv')
# df.to_sql('player_game_stats', db.engine, if_exists='append', index=False, chunksize=1000)

# df = pd.read_csv('historical-data/playType.csv')
# df.to_sql('play_type_stats', db.engine, if_exists='append', index=False)

df = pd.read_csv('historical-data/players.csv')
df.to_sql('player', db.engine, if_exists='append', index=False, chunksize=1000)

df = pd.read_csv('historical-data/teams.csv')
df.to_sql('team', db.engine, if_exists='append', index=False)
