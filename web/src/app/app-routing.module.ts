import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GamesComponent } from './views/game/games/games.component';
import { GameDetailComponent } from './views/game/game-detail/game-detail.component';
import { TeamsComponent } from './views/team/teams/teams.component';
import { TeamDetailComponent } from './views/team/team-detail/team-detail.component';
import { PlayersComponent } from './views/player/players/players.component';
import { PlayerDetailComponent } from './views/player/player-detail/player-detail.component';
import { ConsistencyComponent } from './views/player/consistency/consistency.component';

const routes: Routes = [
  { path: '', redirectTo: '/games', pathMatch: 'full' },
  { path: 'games', component: GamesComponent },
  { path: 'gameDetail/:id', component: GameDetailComponent },
  { path: 'teams', component: TeamsComponent },
  { path: 'teamDetail/:name/:season/:seasonType', component: TeamDetailComponent },
  { path: 'players', component: PlayersComponent },
  { path: 'playerDetail/:name/:season/:seasonType', component: PlayerDetailComponent},
  { path: 'players/consistency', component: ConsistencyComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
