//NPM Imports

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatTooltipModule,
} from '@angular/material';

//Top Level App Imports
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

//Header Footer
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

//Views
import { GamesComponent } from './views/game/games/games.component';
import { GameDetailComponent } from './views/game/game-detail/game-detail.component';

import { TeamsComponent } from './views/team/teams/teams.component';
import { TeamDetailComponent } from './views/team/team-detail/team-detail.component';

import { PlayersComponent } from './views/player/players/players.component';
import { PlayerDetailComponent } from './views/player/player-detail/player-detail.component';

//Components
import { GameRotationsVizComponent } from './components/viz/rotations/game-rotations-viz/game-rotations-viz.component';
import { ScatterShotChartComponent } from './components/viz/shots/scatter-shot-chart/scatter-shot-chart.component';
import { StatsTableComponent } from './components/stats-table/stats-table.component';

import { SeasonRotationsVizComponent } from './components/viz/rotations/season-rotations-viz/season-rotations-viz.component';
import { BinnedShotChartComponent } from './components/viz/shots/binned-shot-chart/binned-shot-chart.component';
import { PlayTypeComponent } from './components/viz/play-type/play-type/play-type.component';
import { PlayTypesComponent } from './components/viz/play-type/play-types/play-types.component';
import { PossessionBreakdownComponent } from './components/viz/possession-breakdown/possession-breakdown.component';
import { ConsistencyComponent } from './views/player/consistency/consistency.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    GamesComponent,
    GameDetailComponent,
    TeamsComponent,
    TeamDetailComponent,
    GameRotationsVizComponent,
    ScatterShotChartComponent,
    StatsTableComponent,
    SeasonRotationsVizComponent,
    BinnedShotChartComponent,
    PlayTypeComponent,
    PlayTypesComponent,
    PlayersComponent,
    PlayerDetailComponent,
    PossessionBreakdownComponent,
    ConsistencyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatListModule,
    MatInputModule,
    MatIconModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    FlexLayoutModule
  ],
  providers: [
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
