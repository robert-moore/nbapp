import { GameSummary } from './game-summary.model';
import { Shot } from './shot.model';
import { Rotation } from './rotation.model';
import { Score } from './score.model';

export class GameDetails {
    summary: GameSummary;
    rotations: Array<Rotation>;
    shots: Array<Shot>;
    score: Array<Score>;
    stats: Array<any>;
}