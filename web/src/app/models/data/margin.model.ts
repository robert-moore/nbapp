export class Margin {
    right: number;
    left: number;
    top: number;
    bottom: number;

    constructor(width) {
        this.right = width/30;
        this.left = width/100;
        this.top = width/100;
        this.bottom = width/15;
    }
}