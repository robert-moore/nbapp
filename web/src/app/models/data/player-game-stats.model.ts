export class PlayerGameStats {
    game_id: number;
    games_played: number;
    player: Player;
    team: Team;
    time: number;
    possessions: number;
    scoring: ScoringStats;
    passing: PassingStats;
    tov: number;
    rebounding: ReboundingStats;
    defense: DefenseStats;
    possession: PossessionStats;
    movement: MovementStats;
    plus_minus: PlusMinusStats;
    screen_ast: number;

    constructor () {
        
    }
}

class Player {
    name: string;
    team: string;
    starter: boolean;
}

class Team {
    pts: number;
    opp_pts: number;
}

class ScoringStats {
    fg2a: number;
    fg2m: number;
    fg3a: number;
    fg3m: number;
    ft1a: number;
    ft2a: number;
    ft3a: number;
    tech_fta: number;
    ftm: number;

}

class PassingStats {
    passes: number;
    ast: number;
    potential_ast: number;
    ft_ast: number;
    secondary_ast: number;
    ast_pts: number;
}

class ReboundingStats {
    oreb: number;
    oreb_chances: number;
    dreb: number;
    dreb_chances: number;
    box_outs: number;
}

class DefenseStats {
    rim_protection: RimProtection;
    shot_contest: ShotContest;
    blk: number;
    stl: number;
    deflections: number;
    recoveries: number;
    charges_drawn: number;
}

class RimProtection {
    dfga: number;
    dfgm: number;
}

class ShotContest {
    fg2: number;
    fg3: number;
}

class PossessionStats {
    time_of_poss: number;
    touches: number;
    elbow: number;
    paint: number;
    post: number;
}

class MovementStats {
    distance: number;
    speed: number;
}

class PlusMinusStats {
    raw: number;
    ortg: number;
    drtg: number;
}