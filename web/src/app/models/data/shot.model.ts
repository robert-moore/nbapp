export class Shot {
    assister: string;
    game_id: number;
    made: boolean;
    shooter: string;
    side: string;
    team: string;
    value: number;
    x: number;
    y: number;
    zone: string;
}