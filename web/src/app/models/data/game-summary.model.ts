export class GameSummary {
    id: number;
    home: TeamGame;
    away: TeamGame;
    date: Date;
    season: number;
    season_type: string;
}

class TeamGame {
    score: number;
    team: string;
}