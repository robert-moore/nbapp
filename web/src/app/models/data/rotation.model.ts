export class Rotation {
    end: number;
    game_id: number;
    player: string;
    start: number;
    team: string;
}