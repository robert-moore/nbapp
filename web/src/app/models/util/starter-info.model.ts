export class StarterInfo {
    players: string[];
    games: number[];

    constructor(players: string[], game: number) {
        this.players = players;
        this.games = [game];
    }
}