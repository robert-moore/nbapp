export class ColumnCategory {
    columns: Array<string>;
    isDisplayed: boolean;

    constructor(columns: Array<string>, isDisplayed = false) {
        this.columns = columns;
        this.isDisplayed = isDisplayed;
    }
}