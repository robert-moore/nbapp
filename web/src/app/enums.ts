export const SEASONS: number[] = [
    2014, 2015, 2016, 2017, 2018
]

export const SEASON_TYPES: string[] = [
    'Regular Season', 'Playoffs'
]


export const TEAM_COLORS: object = {
    'ATL': ['#e03a3e', '#C1D32F'],
    'BKN': ['#000000', '#FFFFFF'],
    'BOS': ['#007A33', '#007A33'],
    'CHA': ['#00788C', '#1D1160'],
    'CHI': ['#CE1141', '#000000'],
    'CLE': ['#6F2633', '#FFB81C'],
    'DAL': ['#00538C', '#002B5e'],
    'DEN': ['#0E2240', '#FEC524'],
    'DET': ['#C8102E', '#006BB6'],
    'GSW': ['#003DA5', '#FFC72C'],
    'HOU': ['#CE1141', '#FDB927'],
    'IND': ['#041E42', '#FFCD00'],
    'LAC': ['#ED174C', '#006BB6'],
    'LAL': ['#552583', '#FDB927'],
    'MEM': ['#7D9BC1', '#FFC72C'],
    'MIA': ['#98012e', '#faa11b'],
    'MIL': ['#00471B', '#EEE1C6'],
    'MIN': ['#002B5C', '#7AC143'],
    'NOP': ['#0C2340', '#C8102E'],
    'NYK': ['#F58426', '#006BB6'],
    'OKC': ['#0072CE', '#F9423A'],
    'ORL': ['#0077c0', '#000000'],
    'PHI': ['#003DA5', '#D50032'],
    'PHX': ['#1d1160', '#e56020'],
    'POR': ['#E13A3E', '#000000'],
    'SAC': ['#5a2d81', '#000000'],
    'SAS': ['#000000'],
    'TOR': ['#BA0C2F', '#000000'],
    'UTA': ['#0C2340', '#2C5234'],
    'WAS': ['#0C2340', '#c8102E'],
};

export const PLAY_TYPES = ['Transition', 'Isolation', 'PRBallHandler', 'PRRollman', 'Postup', 'Spotup', 'Handoff', 'Cut', 'OffScreen', 'OffRebound']