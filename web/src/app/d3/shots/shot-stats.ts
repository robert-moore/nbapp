import * as d3 from 'd3';
import { Shot } from 'src/app/models/data/shot.model';
import { leageZoneInfo, colors, colorScales } from './shots';

const shotStatsContianerWidth = 50, shotStatsContainerHeight = 10;

const zoneColorScales = {
    'RA': d3.scaleQuantize().domain([0.25, 0.45]).range(colors),
    'ShortMid': d3.scaleQuantize().domain([0.10, 0.25]).range(colors),
    'Mid': d3.scaleQuantize().domain([0.05, 0.25]).range(colors),
    'Three': d3.scaleQuantize().domain([0.25, 0.45]).range(colors),
};

const shotStatTextStyle = {
    'size': 0.15,
    'weight': 'bold',
    'anchor': 'middle',
    'stroke': 'black',
    'strokeWidth': '0.05'
};

function calculateExpectedShotValue(zone: string, side: string): number {
    const expectedMakePct = +leageZoneInfo[zone][side]['pct_make'];
    return zone.includes('3') ? expectedMakePct * 3 : expectedMakePct * 2;
}

function calculateActualShotValue(shot: Shot): number {
    return !shot.made ? 0 : (shot.zone.includes('3') ? 3 : 2);
}

export function drawShotStatsContainer(containerSelector: string) {
    d3.select(containerSelector).append('svg')
        .attr('viewBox', '0, 0, ' + shotStatsContianerWidth + ', ' + shotStatsContainerHeight)
        .classed('shotStatsSVG', true);
}

function drawEfgStats(efg: number, svg: any, type: string) {
    const x = (shotStatsContianerWidth / 6) * 5,
        labelY = type === 'Expected' ? 1 : (shotStatsContainerHeight / 2) + 0.5,
        efgY = type === 'Expected' ? ((shotStatsContainerHeight / 4) + 1) : ((shotStatsContainerHeight * 3 / 4) + 1);

    svg.append('text')
        .attr('x', x)
        .attr('y', labelY)
        .text(type + ' Efg')
        .classed('shotStatsText', true)
        .style('font-size', '0.075rem')
        .style('text-anchor', 'middle');

    svg.append('text')
        .attr('x', x)
        .attr('y', efgY)
        .text(efg.toFixed(1))
        .classed('shotStatsText', true)
        .style('font-size', shotStatTextStyle.size + 'rem')
        .style('font-weight', shotStatTextStyle.weight)
        .style('text-anchor', shotStatTextStyle.anchor)
        .style('fill', colorScales['raw'](efg))
        .style('stroke', shotStatTextStyle.stroke)
        .style('stroke-width', shotStatTextStyle.strokeWidth);
}

function drawShotStatsForZone(zone: string, index: number, shotData: Shot[], svg: any) {
    let shotsInZone: Shot[] = [];
    if (zone !== 'Three') {
        shotsInZone = shotData.filter(shot => shot.zone === zone);
    } else {
        shotsInZone = shotData.filter(shot => shot.zone.includes('3'));
    }

    const pctOfTotalShots = shotsInZone.length / shotData.length * 100,
        pctOfShotsMade = shotsInZone.length === 0 ? 0 : shotsInZone.filter(shot => shot.made).length / shotsInZone.length * 100,
        efg = (zone === 'Three' ? pctOfShotsMade * 3 / 2 : pctOfShotsMade) / 100,
        x = (shotStatsContianerWidth / 6) * (index + 1);

    svg.append('text')
        .attr('x', x)
        .attr('y', 1)
        .text(zone === 'RA' ? 'Rim' : zone)
        .classed('shotStatsText', true)
        .style('font-size', '0.075rem')
        .style('text-anchor', 'middle');

    svg.append('text')
        .attr('x', x)
        .attr('y', (shotStatsContainerHeight / 4) + 1)
        .text(pctOfTotalShots === 100 ? 100 : pctOfTotalShots.toFixed(1))
        .classed('shotStatsText', true)
        .style('font-size', shotStatTextStyle.size + 'rem')
        .style('font-weight', shotStatTextStyle.weight)
        .style('text-anchor', shotStatTextStyle.anchor)
        .style('fill', zoneColorScales[zone](pctOfTotalShots / 100))
        .style('stroke', shotStatTextStyle.stroke)
        .style('stroke-width', shotStatTextStyle.strokeWidth);

    svg.append('text')
        .attr('x', x)
        .attr('y', (3 * shotStatsContainerHeight / 4) + 1)
        .text(pctOfShotsMade === 100 ? 100 : pctOfShotsMade.toFixed(1))
        .classed('shotStatsText', true)
        .style('font-size', shotStatTextStyle.size + 'rem')
        .style('font-weight', shotStatTextStyle.weight)
        .style('text-anchor', shotStatTextStyle.anchor)
        .style('fill', colorScales['raw'](efg))
        .style('stroke', shotStatTextStyle.stroke)
        .style('stroke-width', shotStatTextStyle.strokeWidth);
}


export function drawShotStats(shotData: Shot[], selector: string) {
    const container = d3.select(selector);
    container.selectAll('.shotStatsText').remove();
    const svg = container.select('.shotStatsSVG');

    svg.append('text')
        .attr('x', 1.7)
        .attr('y', shotStatsContainerHeight / 4)
        .text('% of')
        .classed('shotStatsText', true)
        .style('font-size', '0.075rem')
        .style('text-anchor', 'middle');

    svg.append('text')
        .attr('x', 1.7)
        .attr('y', shotStatsContainerHeight / 4 + 1)
        .text('Shots')
        .classed('shotStatsText', true)
        .style('font-size', '0.075rem')
        .style('text-anchor', 'middle');

    svg.append('text')
        .attr('x', 1.7)
        .attr('y', shotStatsContainerHeight * 3 / 4 + 0.5)
        .text('FG%')
        .classed('shotStatsText', true)
        .style('font-size', '0.075rem')
        .style('text-anchor', 'middle');

    ['RA', 'ShortMid', 'Mid'].forEach((zone, index) => {
        drawShotStatsForZone(zone, index, shotData, svg);
    });

    drawShotStatsForZone(
        'Three', 3, shotData, svg
    );

    const expectedEfg = shotData
        .map(shot => calculateExpectedShotValue(shot.zone, shot.side))
        .reduce((a, b) => a + b) / shotData.length * 50;

    const actualEfg = shotData
        .map(shot => calculateActualShotValue(shot))
        .reduce((a, b) => a + b) / shotData.length * 50;

    drawEfgStats(expectedEfg, svg, 'Expected');
    drawEfgStats(actualEfg, svg, 'Actual');

    svg.append('line')
        .attr('x1', (shotStatsContianerWidth / 6) * 4.47)
        .attr('x2', (shotStatsContianerWidth / 6) * 4.47)
        .attr('y1', 0)
        .attr('y2', shotStatsContainerHeight - 1)
        .style('stroke-width', 0.1)
        .attr('stroke-opacity', 0.5)
        .style('stroke', 'black')
        .style('fill', 'none');
}