import * as d3 from 'd3';
import { yScale } from './shots';
import { ShotType } from './shot-type.model';
import { Shot } from 'src/app/models/data/shot.model';

const shotRadius = 0.45;
const shotStrokeWidth = 0.05;

const madeShot = new ShotType('red', 'none'),
      missedShot = new ShotType('none', 'red'),
      assist = new ShotType('blue', 'none');

export function drawScatterShots(containerSelector: string, shotData: Shot[], player: string) {

  const shotOpacity = Math.max(1 - (shotData.length / 1000), 0.05);

  const svg = d3.select(containerSelector).select('.court');

  const shotsGroup = svg.select('.shots');

  let shots = shotsGroup.selectAll('.shot')
    .data([]);

  shots.exit()
    .attr('r', 0)
    .remove();

  shots = shotsGroup.selectAll('.shot')
    .data(shotData, function (d) {
      return [d.x, d.y, d.shooter, d.assister];
    });

  shots.enter()
    .append('circle')
    .classed('shot', true)
    .attr('cx', d => 25.5 - d.x / 10)
    .attr('cy', d => yScale(d.y / 10) - 5)
    .attr('r', 0)
    .style('opacity', shotOpacity)
    .style('fill', d => {
      if (d.assister === player) {
        return assist.fill;
      } else if (d.made) {
        return madeShot.fill;
      } else {
        return missedShot.fill;
      }
    })
    .style('stroke', d => d.made ? madeShot.stroke : missedShot.stroke)
    .style('stroke-width', shotStrokeWidth)
    .attr('r', shotRadius);
}

export function drawScatterLegend(containerSelector: string, showAssists: boolean) {

  const court = d3.select(containerSelector).select('.court');

  court.selectAll('.legend').remove();
  const legendElement = court.append('g').attr('class', 'legend');
  const legendHeight = 48,
        legendTextHeightOffset = 0.4,
        legendXStart = showAssists ? 10 : 15;

  legendElement.append('circle')
    .attr('cx', legendXStart)
    .attr('cy', legendHeight)
    .attr('r', shotRadius)
    .style('opacity', 1)
    .style('fill', madeShot.fill);

  legendElement.append('text')
    .attr('x', legendXStart + 0.6)
    .attr('y', legendHeight + legendTextHeightOffset)
    .text('Made Shot')
    .style('font-size', '.08rem')
    .style('text-anchor', 'start');

    legendElement.append('circle')
    .attr('cx', legendXStart + 11)
    .attr('cy', legendHeight)
    .attr('r', shotRadius)
    .style('opacity', 1)
    .style('fill', missedShot.fill)
    .style('stroke', missedShot.stroke)
    .style('stroke-width', shotStrokeWidth);

  legendElement.append('text')
    .attr('x', legendXStart + 11.6)
    .attr('y', legendHeight + legendTextHeightOffset)
    .text('Missed Shot')
    .style('font-size', '.08rem')
    .style('text-anchor', 'start');

  if (showAssists) {
    legendElement.append('circle')
      .attr('cx', legendXStart + 22.5)
      .attr('cy', legendHeight)
      .attr('r', shotRadius)
      .style('opacity', 1)
      .style('fill', assist.fill);

    legendElement.append('text')
      .attr('x', legendXStart + 23.1)
      .attr('y', legendHeight + legendTextHeightOffset)
      .text('Assist')
      .style('font-size', '.08rem')
      .style('text-anchor', 'start');
  }
}