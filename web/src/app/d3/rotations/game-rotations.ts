import * as d3 from 'd3';
import { TEAM_COLORS } from '../../enums';
import { RotationChartDimensions } from '../../models/util/rotation-chart-dimension.model';
import { Rotation } from 'src/app/models/data/rotation.model';
import { GameDetails } from 'src/app/models/data/game-details.model';

const totalHeight = 40,
    totalWidth = 100,
    scoreAxisWidth = 4;

export function sortPlayers(data : GameDetails): String[] {
    const players: String[] = [];

    // sort rotations
    // 1: players on the home team before away team
    // 2: starters before bench
    // 3: sort by minutes
    data.rotations = data.rotations.sort((a, b) => {
        if (a.team !== b.team) {
            return a.team === data.summary.home.team ? -1 : 1;
        } else {
            const aRotations = data.rotations.filter(x => x.player === a.player),
                bRotations = data.rotations.filter(x => x.player === b.player);

            const aIsStarter = aRotations.filter(x => x.start === 0).length > 0,
                bIsStarter = bRotations.filter(x => x.start === 0).length > 0;

            if (aIsStarter !== bIsStarter) {
                return aIsStarter ? -1 : 1;
            } else {
                const aLengths = aRotations.map(x => x.end - x.start),
                    bLengths = bRotations.map(x => x.end - x.start);

                const aMinutes = aLengths.length > 1 ? aLengths.reduce((x, y) => x + y) : aLengths[0], 
                    bMinutes = bLengths.length > 1 ? bLengths.reduce((x, y) => x + y) : bLengths[0];

                return bMinutes - aMinutes;
            }
        }
    });

    // Get unique list of players
    // Get longest player name => used to determine width of left x axis
    // Get max minutes => determine if overtime periods need to be added
    let previousTeam = data.summary.home.team;
    data.rotations.forEach(element => {
        if (players.indexOf(element.player) === -1) {
            if (element.team !== previousTeam) {
                players.push('');
            }
            players.push(element.player);
        }
        previousTeam = element.team;
    });

    return players;
}

export function plotRotations(data: GameDetails, players: String[]): void {
    // remove current chart
    d3.select('#rotation-chart-container').selectAll('svg').remove();

    // append new svg with viewBox
    const svg = d3.select('#rotation-chart-container').append('svg')
        .attr('viewBox', '0, 0, ' + totalWidth + ', ' + totalHeight);

    const home_color = TEAM_COLORS[data.summary.home.team][0],
        away_color = TEAM_COLORS[data.summary.away.team][0];

    // find the last minute logged: determines overtime
    // calculate longest player name: determines width of player labels
    const maxMinute = data.rotations.map(r => r.end).reduce((a: number, b: number) => a > b ? a : b) / 60,
        longestPlayerName = players.reduce((a: string, b: string) => a.length > b.length ? a : b),
        playerLabelsWidth = (longestPlayerName.length + 1) * 0.75;

    // calculate height of each player row. height over number of players plus top and bottom minute labels
    // calculate scale used to determine width of rotation rects
    const rectHeight = totalHeight / (players.length + 4),
        x_scale = (totalWidth - scoreAxisWidth - playerLabelsWidth) / (maxMinute * 60);

    // Add Player Labels
    svg.selectAll('.playerLabel')
        .data(players)
        .enter().append('text')
        .text(d => d)
        .attr('x', playerLabelsWidth)
        .attr('y', (d, index) => (index + 2) * rectHeight)
        .attr('font-size', rectHeight * 0.75 + 'px')
        .style('text-anchor', 'end');

    // Add Quarter Labels
    const minutes = ['Q1', '', '', '', '', '', '', '', '', '', '', '', 'Q2', '', '', '', '', '', '', '', '', '', '', '',
        'Q3', '', '', '', '', '', '', '', '', '', '', '', 'Q4', '', '', '', '', '', '', '', '', ''];
    if (maxMinute >= 48) {
        const extraMinutes = maxMinute - 48;
        minutes.push('');
        for (let i = 0; i < extraMinutes; i++) {
            if (i % 5 === 0) {
                minutes.push('OT' + (i / 5 + 1));
            } else {
                minutes.push('');
            }
        }
    }
    minutes.push('end');

    svg.selectAll('.quarterLabelTop')
        .data(minutes)
        .enter().append('text')
        .text(d => d)
        .attr('x', (d, i) => (i + 1) * x_scale * 60 + playerLabelsWidth)
        .attr('y', rectHeight)
        .attr('font-size', rectHeight * 0.75 + 'px')
        .style('text-anchor', 'end');

    svg.selectAll('.quarterLabelBottom')
        .data(minutes)
        .enter().append('text')
        .text(d => d)
        .attr('x', (d, i) => (i + 1) * x_scale * 60 + playerLabelsWidth)
        .attr('y', (players.length + 2) * rectHeight)
        .attr('font-size', rectHeight * 0.75 + 'px')
        .style('text-anchor', 'end');

    const cards = svg.selectAll('.rotation').data(data.rotations);

    cards.append('title');

    cards.enter().append('rect')
        .attr('x', d => d.start * x_scale + playerLabelsWidth)
        .attr('y', d => (players.indexOf(d.player) + 1.3) * rectHeight)
        .attr('rx', 0.25)
        .attr('ry', 0.25)
        .attr('width', d => (d.end - d.start) * x_scale)
        .attr('height', rectHeight * .8)
        .style('fill', 'black');


    const maxLead = Math.abs(data.score.map(s => s.margin).reduce((a, b) => Math.abs(a) > Math.abs(b) ? a : b));

    const botTeamPlayerCount = players.length - players.indexOf('') - 1,
        topTeamPlayerCount = players.indexOf('');

    const yAxisShiftBool = botTeamPlayerCount < topTeamPlayerCount,
        yAxisScale = Math.min(topTeamPlayerCount, botTeamPlayerCount) * 2 * rectHeight,
        yAxisShift = yAxisShiftBool ? (topTeamPlayerCount - botTeamPlayerCount + 1.5) * rectHeight : rectHeight * 1.3;

    const x = d3.scaleLinear()
        .rangeRound([0, 96 - playerLabelsWidth]);

    const y = d3.scaleLinear()
        .range([0, yAxisScale]);

    y.domain([Math.max(maxLead, 20), Math.min(-maxLead, -20)]);

    svg.append('g')
        .style('font-size', rectHeight * 0.75 + 'px')
        .call(d3.axisRight(y).tickSize(0))
        .attr('transform', 'translate(94, ' + (yAxisShift) + ')')
        .append('text')
        .attr('fill', '#000')
        .attr('transform', 'rotate(-90)')
        .attr('y', 0)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end');

    let score_datas = [];
    let current_data = [],
        current_score = 0;

    data.score.sort((a, b) => a.minute - b.minute);

    data.score.forEach(element => {
        if (element.margin === 0) {
            current_data.push(element);
        } else if ((current_score * element.margin) < 0) {
            score_datas.push(current_data);
            current_data = [element];
            current_score = element.margin;
        } else {
            current_data.push(element);
            current_score = element.margin;
        }
    });
    score_datas.push(current_data);

    const line = d3.area()
        .x(d => x(d.minute))
        .y0(d => y(0))
        .y1(d => y(d.margin))
        .curve(d3.curveStep);

    x.domain(d3.extent(data.score, d => +d.minute));

    score_datas = score_datas.filter(x => x.length > 0);

    for (let x = 0; x < score_datas.length; x++) {
        score_datas[x] = score_datas[x].sort((a, b) => a.minute - b.minute);
    }

    score_datas.forEach(element => {
        const element_scores = element.map(e => e.margin),
            avg_score = element_scores.reduce((a, b) => a + b) / element_scores.length, 
            line_color = avg_score > 0 ? home_color : away_color;

        svg.append('path')
            .datum(element)
            .attr('class', 'point-diff')
            .attr('fill', line_color)
            .attr('stroke', line_color)
            .attr('stroke-opacity', 1)
            .attr('stroke-linejoin', 'bevel')
            .attr('stroke-linecap', 'square')
            .attr('stroke-width', 0.15)
            .attr('opacity', 0.5)
            .attr('d', line)
            .attr('transform', 'translate(' + playerLabelsWidth + ',' + (yAxisShift + 0.5) + ')');
    });

    const strokeWidth = 0.2,
        strokeOpacity = 0.25,
        strokeColor = 'black';

    for (let i = 0; i < 4; i++) {
        svg.append('line')
            .attr('x1', (720 * i * x_scale) + playerLabelsWidth)
            .attr('x2', (720 * i * x_scale) + playerLabelsWidth)
            .attr('y1', rectHeight * 1.5)
            .attr('y2', totalHeight - rectHeight * 3)
            .style('stroke-width', strokeWidth)
            .attr('stroke-opacity', strokeOpacity)
            .style('stroke', strokeColor)
            .style('fill', 'none');
    }

    if (maxMinute > 48) {
        for (let i = 0; i < ((maxMinute - 48) / 5); i++) {
            svg.append('line')
                .attr('x1', ((2880 + (300 * i)) * x_scale) + playerLabelsWidth)
                .attr('x2', ((2880 + (300 * i)) * x_scale) + playerLabelsWidth)
                .attr('y1', rectHeight * 1.5)
                .attr('y2', totalHeight - rectHeight * 3)
                .style('stroke-width', strokeWidth)
                .attr('stroke-opacity', strokeOpacity)
                .style('stroke', strokeColor)
                .style('fill', 'none');
        }
    }

    svg.append('rect')
        .attr('x', playerLabelsWidth)
        .attr('y', totalHeight - rectHeight * 1.5)
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('width', totalWidth / 10)
        .attr('height', rectHeight)
        .style('fill', 'black');

    svg.append('text')
        .attr('x', playerLabelsWidth)
        .attr('y', totalHeight - rectHeight * 0.75)
        .text('Player On Court')
        .style('font-size', rectHeight * 0.75 + 'px')
        .style('text-anchor', 'end');

    svg.append('rect')
        .attr('x', playerLabelsWidth + totalWidth / 10)
        .attr('y', totalHeight - rectHeight)
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('width', totalWidth / 40)
        .attr('height', rectHeight)
        .attr('opacity', 0.5)
        .style('fill', away_color);

    svg.append('text')
        .attr('x', playerLabelsWidth + totalWidth * 5 / 40)
        .attr('y', totalHeight - rectHeight * 0.25)
        .style('font-size', rectHeight * 0.75 + 'px')
        .text(data.summary.away.team + ' Lead');

    svg.append('rect')
        .attr('x', playerLabelsWidth + totalWidth / 10)
        .attr('y', totalHeight - rectHeight * 2)
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('width', totalWidth / 40)
        .attr('height', rectHeight)
        .attr('opacity', 0.5)
        .style('fill', home_color);

    svg.append('text')
        .attr('x', playerLabelsWidth + totalWidth * 5 / 40)
        .attr('y', totalHeight - rectHeight)
        .style('font-size', rectHeight * 0.75 + 'px')
        .text(data.summary.home.team + ' Lead');
}
