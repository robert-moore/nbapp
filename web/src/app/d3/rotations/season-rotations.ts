import * as d3 from 'd3';
import { RotationChartDimensions } from '../../models/util/rotation-chart-dimension.model';
import { Rotation } from '../../models/data/rotation.model';
import { StarterInfo } from '../../models/util/starter-info.model';


export function sortPlayers(data: Rotation[]): String[] {
    const players = Array.from(new Set(data.map(r => r.player)));
    players.sort((a: string, b: string) => getTotalMinutesForPlayer(data, b) - getTotalMinutesForPlayer(data, a));
    return players;
}

export function plotRotations(data, players) {
    d3.select('#rotationsChartContainer').select('.rotationsChart').remove();
    const svg = drawPlot();

    const longestPlayerName = players.reduce((a: string, b: string) => a.length > b.length ? a : b),
        games = Array.from(new Set(data.map(x => x.game_id)));

    const playerLabelsWidth = longestPlayerName.length + 1;

    const maxWidth = (100 - playerLabelsWidth) / 48,
        maxHeight = 40 / (players.length + 2.5),
        gridSize = Math.min(maxWidth, maxHeight);

    svg.selectAll('.playerLabel')
        .data(players)
        .enter().append('text')
        .text(d => d)
        .attr('x', playerLabelsWidth)
        .attr('y', (d, index) => (index + 2) * gridSize)
        .style('font-size', gridSize + 'px')
        .style('text-anchor', 'end');

    const minutes = ['Q1', '', '', '', '', '', '', '', '', '', '', '', 'Q2', '', '', '', '', '', '', '', '', '', '', '',
        'Q3', '', '', '', '', '', '', '', '', '', '', '', 'Q4', '', '', '', '', '', '', '', '', '', '', 'end'];

    svg.selectAll('.quarterLabelTop')
        .data(minutes)
        .enter().append('text')
        .text(d => d)
        .attr('x', (d, index) => (index + 1) * gridSize + playerLabelsWidth)
        .attr('y', gridSize)
        .style('font-size', gridSize + 'px')
        .style('text-anchor', 'end');

    svg.selectAll('.quarterLabelBottom')
        .data(minutes)
        .enter().append('text')
        .text(d => d)
        .attr('x', (d, index) => (index + 1) * gridSize + playerLabelsWidth)
        .attr('y', (players.length + 2) * gridSize)
        .style('font-size', gridSize + 'px')
        .style('text-anchor', 'end');

    const grid = [];
    players.forEach((player) => {
        for (let i = 0; i < 48; i++) {
            grid.push({ player: player, minute: i });
        }
    });

    const gridBoxes = svg.selectAll('.playerMinute').data(grid);

    gridBoxes.enter().append('rect')
        .attr('x', d => d.minute * gridSize + playerLabelsWidth)
        .attr('y', d => (players.indexOf(d.player) + 1.1) * gridSize)
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('class', 'hour bordered')
        .attr('width', gridSize)
        .attr('height', gridSize)
        .attr('opacity', d => getOpacity(data, d.player, d.minute, games.length))
        .style('fill', 'black');
}

function drawPlot(): any {
    return d3.select('#rotationsChartContainer').append('svg')
        .attr('viewBox', '0, 0, 100, 40')
        .classed('rotationsChart', true);
}

function getOpacity(rotations: Rotation[], player: string, minute: number, num_games: number): number {
    let seconds_played_in_minute = 0;
    const playersRotations = rotations.filter((rotation: Rotation) => rotation.player === player);

    playersRotations.forEach(rotation => {
        const start = Math.max(rotation.start, minute * 60),
            end = Math.min(rotation.end, (minute + 1) * 60);
        seconds_played_in_minute += Math.max(0, end - start);
    });

    return seconds_played_in_minute / (num_games * 60);
}

export function getStartingLineups(rotations: Rotation[]): StarterInfo[] {
    const gameIds = Array.from(new Set(rotations.map(x => x.game_id)));
    const starters: StarterInfo[] = [];

    gameIds.forEach(g => {
        const gameStartingRotations = rotations.filter(r => r.start === 0 && r.game_id === g),
            gameStarters = gameStartingRotations.map(gsr => gsr.player).sort();

        const index = starters.findIndex(s => compareArrays(s.players, gameStarters));
        if (index === -1) {
            starters.push(new StarterInfo(gameStarters, g));
        } else {
            starters[index]['games'].push(g);
        }
    });
    starters.sort((a: StarterInfo, b: StarterInfo) => b.games.length - a.games.length);
    return starters;
}

function getTotalMinutesForPlayer(rotationData: Rotation[], player: string): number {
    const playerRotations = rotationData.filter((r: Rotation) => r.player === player);
    return playerRotations.map(pr => (pr.end - pr.start)).reduce((a, b) => a + b);
}

function compareArrays(a: any[], b: any[]): boolean {
    let eqaulity = true;
    if (a.length !== b.length) {
        return false;
    } else {
        for (let i = 0; i < a.length; i++) {
            if (a[i] !== b[i]) {
                eqaulity = false;
                break;
            }
        }
    }
    return eqaulity;
}