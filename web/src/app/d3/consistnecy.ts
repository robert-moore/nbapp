import * as d3 from 'd3';
import { TEAM_COLORS } from '../enums';

const width = 100,
    height = 45;

const fontSize = 0.75;

export function plotConsistency(data) {
    const container = d3.select('#consistencyContainer');
    const svg = container.append('svg')
        .attr('viewBox', '0, 0, ' + width + ', ' + height);

    const maxPlayerNameLength = Math.floor((width / data.length) * 2);

    svg.selectAll('.playerLabel')
        .data(data)
        .enter()
        .append('text')
        .text(d => getDisplayName(d.player.name, maxPlayerNameLength))
        .attr('x', (d, i) => (i + 1) * (width / (data.length + 0.5)))
        .attr('y', d => height - 5)
        .style('font-size', fontSize + 'px')
        .style('text-anchor', 'middle');

    const data_points = [];
    data.forEach((player, index) => {
        player.list.forEach(game => {
            data_points.push(
                {
                    'x': index,
                    'y': game,
                    'num': player.list.filter(g => g === game).length,
                    'inst': data_points.filter(d => d.x === index && d.y === game).length + 1,
                    'color': TEAM_COLORS[player.player.team[0]][0]
                }
            );
        });
    });
    const maxStat = data_points.map(x => x.y).reduce((a, b) => a > b ? a : b);

    svg.selectAll('.gamePoint')
        .data(data_points)
        .enter().append('circle')
        .attr('cx', d => {
            const playerShift = d.x,
                instShift = (d.num / 2) - d.inst,
                xScale = (width / (data.length + 0.5));
            return ((playerShift + 1) * xScale) + (instShift * 0.6);
        })
        .attr('cy', d => ((1 - (d.y / maxStat)) * (height - 7)) + 1)
        .attr('r', 0.3)
        .style('fill', d => d.color);

    const y = d3.scaleLinear().range([0, height - 7]);

    y.domain([maxStat, 0]);

    svg.append('g')
        .attr('transform', 'translate(4,0)')
        .style('font-size', fontSize + 'px')
        .call(d3.axisLeft(y).tickSize(0));
}

export function clearChart() {
    const container = d3.select('#consistencyContainer');
    container.selectAll('svg').remove();
}

function getDisplayName(fullName: string, maxLength: number): string {
    if (fullName.length > maxLength) {
        const firstName = fullName.split(' ')[0],
              lastName = fullName.split(' ')[1];
        const fullLast = firstName[0] + '. ' + lastName;
        const fullFirst = firstName + ' ' + lastName[0] + '.';

        if (fullLast.length < maxLength) {
            return fullLast;
        } else if (fullFirst.length < maxLength) {
            return fullFirst;
        } else {
            return fullLast.slice(0, maxLength);
        }
    }
    return fullName;
}