import * as d3 from 'd3';
import { GameSummary } from '../models/data/game-summary.model';
import { PlayerGameStats } from '../models/data/player-game-stats.model';
import { colorScales, colors } from '../d3/shots/shots';

const height = 12, width = 100;
const teamLabelWidth = 7;

function getTextColor(eff: number) {
    if (eff < 0.35 || eff > 0.65) {
        return 'white';
    } else {
        return 'black';
    }
}

export function plotPossessions(containerSelector: string, summary: GameSummary, stats: PlayerGameStats[]) {
    const container = d3.select(containerSelector),
        svg = container.append('svg')
            .attr('viewBox', '0, 0, ' + width + ', ' + height);

    const teams = [summary.home.team, summary.away.team];

    svg.append('svg:image')
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', 5)
        .attr('height', 5)
        .attr('xlink:href', 'assets/img/' + summary.home.team + '.png');

    svg.append('svg:image')
        .attr('x', 0)
        .attr('y', 6)
        .attr('width', 5)
        .attr('height', 5)
        .attr('xlink:href', 'assets/img/' + summary.away.team + '.png');

    const teamStats = [];

    teams.forEach(t => {
        const filteredStats = stats.filter(s => s.player.team === t);
        const poss = Math.round(filteredStats.map(s => s.possessions).reduce((a, b) => a + b) / 5);

        const teamStatsObj = {
            'fg2a': filteredStats.map(s => s.scoring.fg2a).reduce((a, b) => a + b),
            'fg2m': filteredStats.map(s => s.scoring.fg2m).reduce((a, b) => a + b),
            'fg3a': filteredStats.map(s => s.scoring.fg3a).reduce((a, b) => a + b),
            'fg3m': filteredStats.map(s => s.scoring.fg3m).reduce((a, b) => a + b),
            'ft': filteredStats.map(s => s.scoring.ft2a / 2 + s.scoring.ft3a / 3).reduce((a, b) => a + b),
            'ftm': filteredStats.map(s => s.scoring.ftm).reduce((a, b) => a + b),
            'tov': filteredStats.map(s => s.tov).reduce((a, b) => a + b),
            'oreb': filteredStats.map(s => s.rebounding.oreb).reduce((a, b) => a + b),
            'poss': poss,
            'team': t
        }

        teamStats.push(teamStatsObj);
    });

    const xScale = teamStats.map(t => t.poss + t.oreb).reduce((a, b) => a > b ? a : b);

    const strokeWidth = 0.15;

    svg.selectAll('.poss')
        .data(teamStats).enter()
        .append('rect')
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('x', teamLabelWidth)
        .attr('y', (d, i) => i * (height / 2) + strokeWidth)
        .attr('width', d => (d.poss / xScale) * (width - teamLabelWidth) - strokeWidth)
        .attr('height', height / 5)
        .attr('fill', 'black')
        .style('stroke', 'black')
        .style('opacity', 0.8)
        .style('stroke-width', strokeWidth);

    svg.selectAll('.possText')
        .data(teamStats).enter()
        .append('text')
        .attr('x', teamLabelWidth)
        .attr('y', (d, i) => i * (height / 2) + (height / 10) + strokeWidth)
        .text(d => d.poss + ' Possesions')
        .attr('font-size', '1px')
        .attr('fill', 'white')
        .attr('alignment-baseline', 'middle');

    svg.selectAll('.oreb')
        .data(teamStats).enter()
        .append('rect')
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('x', d => teamLabelWidth + (d.poss / xScale) * (width - teamLabelWidth))
        .attr('y', (d, i) => i * (height / 2) + strokeWidth)
        .attr('width', d => (d.oreb / xScale) * (width - teamLabelWidth) - strokeWidth)
        .attr('height', height / 5)
        .attr('fill', 'grey')
        .style('stroke', 'black')
        .style('opacity', 0.8)
        .style('stroke-width', strokeWidth);

    svg.selectAll('.orebText')
        .data(teamStats).enter()
        .append('text')
        .attr('x', d => teamLabelWidth + (d.poss / xScale) * (width - teamLabelWidth))
        .attr('y', (d, i) => i * (height / 2) + (height / 10) + strokeWidth)
        .text(d => d.oreb + ' Oreb')
        .attr('font-size', '1px')
        .attr('fill', 'white')
        .attr('alignment-baseline', 'middle');

    svg.selectAll('.fg2')
        .data(teamStats).enter()
        .append('rect')
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('x', teamLabelWidth)
        .attr('y', (d, i) => i * (height / 2) + (height / 5) + strokeWidth)
        .attr('width', d => (d.fg2a / xScale) * (width - teamLabelWidth) - strokeWidth)
        .attr('height', height / 5)
        .attr('fill', d => colorScales['raw'](d.fg2m / d.fg2a))
        .style('stroke', 'black')
        .style('opacity', 0.8)
        .style('stroke-width', strokeWidth);

    svg.selectAll('.fg2Text')
        .data(teamStats).enter()
        .append('text')
        .attr('x', teamLabelWidth)
        .attr('y', (d, i) => i * (height / 2) + (height / 5) + (height / 10) + strokeWidth)
        .text(d => d.fg2a + ' 2pt FGA')
        .attr('font-size', '1px')
        .attr('fill', d => getTextColor(d.fg2m / d.fg2a))
        .attr('alignment-baseline', 'middle');

    svg.selectAll('.fg3')
        .data(teamStats).enter()
        .append('rect')
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('x', d => teamLabelWidth + (d.fg2a / xScale) * (width - teamLabelWidth))
        .attr('y', (d, i) => i * (height / 2) + (height / 5) + strokeWidth)
        .attr('width', d => (d.fg3a / xScale) * (width - teamLabelWidth) - strokeWidth)
        .attr('height', height / 5)
        .attr('fill', d => colorScales['raw'](d.fg3m / d.fg3a * 1.5))
        .style('stroke', 'black')
        .style('opacity', 0.8)
        .style('stroke-width', strokeWidth);

    svg.selectAll('.fg3Text')
        .data(teamStats).enter()
        .append('text')
        .attr('x', d => teamLabelWidth + (d.fg2a / xScale) * (width - teamLabelWidth))
        .attr('y', (d, i) => i * (height / 2) + (height / 5) + (height / 10) + strokeWidth)
        .text(d => d.fg3a + ' 3pt FGA')
        .attr('font-size', '1px')
        .attr('fill', d => getTextColor(d.fg3m / d.fg3a * 1.5))
        .attr('alignment-baseline', 'middle');

    svg.selectAll('.ft')
        .data(teamStats).enter()
        .append('rect')
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('x', d => teamLabelWidth + ((d.fg2a + d.fg3a) / xScale) * (width - teamLabelWidth))
        .attr('y', (d, i) => i * (height / 2) + (height / 5) + strokeWidth)
        .attr('width', d => (d.ft / xScale) * (width - teamLabelWidth) - strokeWidth)
        .attr('height', height / 5)
        .attr('fill', d => colorScales['raw'](d.ftm / d.ft * 0.5))
        .style('stroke', 'black')
        .style('opacity', 0.8)
        .style('stroke-width', strokeWidth);

    svg.selectAll('.ftText')
        .data(teamStats).enter()
        .append('text')
        .attr('x', d => teamLabelWidth + ((d.fg2a + d.fg3a) / xScale) * (width - teamLabelWidth))
        .attr('y', (d, i) => i * (height / 2) + (height / 5) + (height / 10) + strokeWidth)
        .text(d => d.ft + ' FT trips')
        .attr('font-size', '1px')
        .attr('fill', 'white')
        .attr('alignment-baseline', 'middle');

    svg.selectAll('.tov')
        .data(teamStats).enter()
        .append('rect')
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('x', d => teamLabelWidth + ((d.fg2a + d.fg3a + d.ft) / xScale) * (width - teamLabelWidth))
        .attr('y', (d, i) => i * (height / 2) + (height / 5) + strokeWidth)
        .attr('width', d => (d.tov / xScale) * (width - teamLabelWidth) - strokeWidth)
        .attr('height', height / 5)
        .attr('fill', d => colorScales['raw'](0))
        .style('stroke', 'black')
        .style('opacity', 0.8)
        .style('stroke-width', strokeWidth);

    svg.selectAll('.tovText')
        .data(teamStats).enter()
        .append('text')
        .attr('x', d => teamLabelWidth + ((d.fg2a + d.fg3a + d.ft) / xScale) * (width - teamLabelWidth))
        .attr('y', (d, i) => i * (height / 2) + (height / 5) + (height / 10) + strokeWidth)
        .text(d => d.tov + ' TOV')
        .attr('font-size', '1px')
        .attr('fill', 'white')
        .attr('alignment-baseline', 'middle');

    const legendColors = svg.selectAll('legendColors').data(colors, d => d);

    legendColors.enter().append('rect')
        .attr('x', (d, i) => i + width - 20)
        .attr('y', height - 1)
        .attr('width', 1)
        .attr('height', 1)
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .style('opacity', 0.8)
        .style('fill', (d, i) => colors[i]);

    svg.append('text')
        .style('font-size', 0.75 + 'px')
        .attr('x', width - 26.5)
        .attr('y', height - 0.25)
        .text('Less Efficient');

    svg.append('text')
        .style('font-size', 0.75 + 'px')
        .attr('x', width - 8.5)
        .attr('y', height - 0.25)
        .text('More Efficient');
}