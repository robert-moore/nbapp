import * as d3 from 'd3';

export function plotPlayType(pt: string, data: any, name: string): void {

    data = data.filter(x => x.poss > 20);

    const width = 300, height = 300
    const container = d3.select('#' + pt + '-play-type-container');
    container.selectAll('svg').remove();
    const svg = container.append('svg')
        .attr('width', width)
        .attr('height', height);

    const maxPpp = Math.max.apply(Math, data.map(function (o) { return o.ppp }));
    const minPpp = Math.min.apply(Math, data.map(function (o) { return o.ppp }));
    const maxFreq = Math.max.apply(Math, data.map(function (o) { return o.possG }));
    const minFreq = Math.min.apply(Math, data.map(function (o) { return o.possG }));

    const xAxis = d3.scaleLinear().rangeRound([20, width - 20]);
    const yAxis = d3.scaleLinear().rangeRound([20, height - 20]);

    xAxis.domain([minFreq, maxFreq]);
    yAxis.domain([maxPpp, minPpp]);

    // xAxis.domain([0, 25]);
    // yAxis.domain([1.5, 0.5]);

    if (pt === 'Transition') {

        svg.append('text')
            .attr('x', 0)
            .attr('y', 0)
            .attr('transform', 'translate(15,' + (height / 2) + ') rotate(-90)')
            .attr('text-anchor', 'middle')
            .classed('playTypeText', true)
            .text('Points Per Possession');

        svg.append('text')
            .attr('x', 0)
            .attr('y', 0)
            .attr('transform', 'translate(' + (width / 2) + ',' + (height - 5) + ')')
            .attr('text-anchor', 'middle')
            .classed('playTypeText', true)
            .text('Possessions Per Game');
    }

    svg.append('g')
        .call(d3.axisLeft(yAxis).ticks(2).tickSize(0).tickValues(yAxis.domain()))
        .attr('transform', 'translate(25,0)')
        .classed('playTypeText', true);

    svg.append('g')
        .call(d3.axisBottom(xAxis).ticks(2).tickSize(0).tickValues(xAxis.domain()).tickFormat(d => Math.round(d)))
        .attr('transform', 'translate(0,' + (height - 15) + ')')
        .classed('playTypeText', true);

    svg.selectAll('.teamPlayType')
        .data(data, function (d) {
            return [d.team, d.freq, d.ppp, d.possG];
        }).enter()
        .append('circle')
        .attr('cx', function (d) {
            return xAxis(d.possG);
        })
        .attr('cy', function (d) {
            return yAxis(d.ppp);
        })
        .attr('r', function (d) {
            return d.name === name ? 7 : 5;
        })
        .attr('opacity', function (d) {
            return d.name === name ? 1 : 0.3;
        })
        .style('fill', function (d) {
            return d.name === name ? 'blue' : 'black';
        })
}