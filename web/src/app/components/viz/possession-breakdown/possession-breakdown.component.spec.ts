import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PossessionBreakdownComponent } from './possession-breakdown.component';

describe('PossessionBreakdownComponent', () => {
  let component: PossessionBreakdownComponent;
  let fixture: ComponentFixture<PossessionBreakdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PossessionBreakdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PossessionBreakdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
