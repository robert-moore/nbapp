import { Component, OnInit, Input } from '@angular/core';
import { plotPossessions } from 'src/app/d3/possession-breakdown';
import { GameSummary } from 'src/app/models/data/game-summary.model';
import { PlayerGameStats } from 'src/app/models/data/player-game-stats.model';

@Component({
  selector: 'possession-breakdown',
  templateUrl: './possession-breakdown.component.html',
  styleUrls: ['./possession-breakdown.component.css']
})
export class PossessionBreakdownComponent implements OnInit {

  @Input() summary: GameSummary;
  @Input() stats: PlayerGameStats[];

  containerSelector = '#possessionBreakDownContainer';

  constructor() { }

  ngOnInit() {
    plotPossessions(this.containerSelector, this.summary, this.stats);
  }

}
