import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameRotationsVizComponent } from './game-rotations-viz.component';

describe('RotationsVizComponent', () => {
  let component: GameRotationsVizComponent;
  let fixture: ComponentFixture<GameRotationsVizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameRotationsVizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameRotationsVizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
