import { Component, OnInit, Input } from '@angular/core';
import { GameDetails } from '../../../../models/data/game-details.model';
import { plotRotations, sortPlayers } from '../../../../d3/rotations/game-rotations';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'game-rotations-viz',
  templateUrl: './game-rotations-viz.component.html',
  styleUrls: ['./game-rotations-viz.component.css']
})
export class GameRotationsVizComponent implements OnInit {

  @Input() game: GameDetails;

  homePlayers: String[] = new Array();
  awayPlayers: String[] = new Array();

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('arrow.up', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow.up.svg'));
    iconRegistry.addSvgIcon('arrow.down', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow.down.svg'));
  }

  ngOnInit() {
    const players = sortPlayers(this.game);

    let isHome: Boolean = true;
    players.forEach((p) => {
      if (p === '') {
        isHome = false;
      } else if (isHome) {
        this.homePlayers.push(p);
      } else {
        this.awayPlayers.push(p);
      }
    });

    plotRotations(this.game, players);
  }

  movePlayer(player: string, up: boolean) {
    const homeIndex = this.homePlayers.indexOf(player);
    if (homeIndex > -1) {
      if (up && homeIndex !== 0) {
        this.homePlayers.splice(homeIndex, 1)[0];
        this.homePlayers.splice(homeIndex - 1, 0, player);
      } else if (!up && homeIndex !== this.homePlayers.length) {
        this.homePlayers.splice(homeIndex, 1)[0];
        this.homePlayers.splice(homeIndex + 1, 0, player);
      }
    } else {
      const awayIndex = this.awayPlayers.indexOf(player);
      if (up && awayIndex !== 0) {
        this.awayPlayers.splice(awayIndex, 1)[0];
        this.awayPlayers.splice(awayIndex - 1, 0, player);
      } else if (!up && awayIndex !== this.awayPlayers.length) {
        this.awayPlayers.splice(awayIndex, 1)[0];
        this.awayPlayers.splice(awayIndex + 1, 0, player);
      }
    }
  }

  reorderPlayers() {
    let players = new Array();
    players.push(...this.homePlayers);
    players.push('');
    players.push(...this.awayPlayers);
    plotRotations(this.game, players);
  }

}
