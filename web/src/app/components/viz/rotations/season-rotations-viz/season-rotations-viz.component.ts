import { Component, OnInit, Input } from '@angular/core';
import { Rotation } from '../../../../models/data/rotation.model';
import { StarterInfo } from '../../../../models/util/starter-info.model';
import { plotRotations, getStartingLineups, sortPlayers } from '../../../../d3/rotations/season-rotations';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'season-rotations-viz',
  templateUrl: './season-rotations-viz.component.html',
  styleUrls: ['./season-rotations-viz.component.css']
})
export class SeasonRotationsVizComponent implements OnInit {

  @Input() data: Rotation[];
  startingLineups: StarterInfo[];
  selectedStartingLinup: StarterInfo;
  allPlayers: String[];
  currentPlayers: String[];

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('arrow.up', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow.up.svg'));
    iconRegistry.addSvgIcon('arrow.down', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow.down.svg'));
  }

  ngOnInit() {
    this.init(this.data);
  }

  public init(data) {
    this.data = data;
    this.startingLineups = getStartingLineups(data);
    this.allPlayers = sortPlayers(data);
    this.currentPlayers = this.allPlayers;
    plotRotations(data, this.currentPlayers);
  }

  starterDisplay(starters: StarterInfo): string {
    let displayString = '';
    starters.players.forEach(p => displayString += p.split(' ')[1] + ', ');
    displayString = displayString.slice(0, -2);
    return displayString + ': ' + starters.games.length + ' games';
  }

  filterByStarters(): void {
    let filteredRotations = [];
    if (this.selectedStartingLinup) {
      filteredRotations = this.data.filter(d => this.selectedStartingLinup.games.indexOf(d.game_id) !== -1);
    } else {
      filteredRotations = this.data;
    }
    let filteredPlayers = sortPlayers(filteredRotations);
    plotRotations(filteredRotations, filteredPlayers);
  }

  movePlayer(player: string, up: boolean) {
    const index = this.currentPlayers.indexOf(player);
    if (up && index !== 0) {
      this.currentPlayers.splice(index, 1)[0];
      this.currentPlayers.splice(index - 1, 0, player);
    } else if (!up && index !== this.currentPlayers.length) {
      this.currentPlayers.splice(index, 1)[0];
      this.currentPlayers.splice(index + 1, 0, player);
    }
  }

  reorderPlayers() {
    plotRotations(this.data, this.currentPlayers);
  }

}
