import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonRotationsVizComponent } from './season-rotations-viz.component';

describe('RotationsVizComponent', () => {
  let component: SeasonRotationsVizComponent;
  let fixture: ComponentFixture<SeasonRotationsVizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeasonRotationsVizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonRotationsVizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
