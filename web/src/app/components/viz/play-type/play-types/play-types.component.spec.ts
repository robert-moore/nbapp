import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayTypesComponent } from './play-types.component';

describe('PlayTypesComponent', () => {
  let component: PlayTypesComponent;
  let fixture: ComponentFixture<PlayTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
