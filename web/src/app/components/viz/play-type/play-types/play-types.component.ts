import { Component, OnInit, Input, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PLAY_TYPES } from '../../../../enums';
import { PlayTypeComponent } from '../play-type/play-type.component';

@Component({
  selector: 'play-types',
  templateUrl: './play-types.component.html',
  styleUrls: ['./play-types.component.css']
})
export class PlayTypesComponent implements OnInit {

  @Input() data: any;
  @ViewChildren('playType') playTypeComponents;
  playTypes = PLAY_TYPES;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  public init(data) {
    let ptcs: PlayTypeComponent[] = this.playTypeComponents._results;
    for (let x = 0; x < ptcs.length; x++) {
      let ptc = ptcs[x];
      ptc.init(data);
    }
  }

}
