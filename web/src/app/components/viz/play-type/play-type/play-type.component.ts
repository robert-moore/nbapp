import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { plotPlayType } from '../../../../d3/play-type';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'play-type',
  templateUrl: './play-type.component.html',
  styleUrls: ['./play-type.component.css']
})
export class PlayTypeComponent implements OnInit, AfterViewInit {

  @Input() playType: string;
  @Input() team: string;
  @Input() data: any;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.data = this.data.filter(d => d.type === this.playType);
  }

  ngAfterViewInit() {
    plotPlayType(this.playType, this.data, this.route.snapshot.paramMap.get('name'));
  }

  public init(data) {
    data = data.filter(d => d.type === this.playType);
    plotPlayType(this.playType, data, this.route.snapshot.paramMap.get('name'));
  }

}
