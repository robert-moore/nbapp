import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Shot } from '../../../../models/data/shot.model';
import { ShotGrid } from '../../../../models/util/shot-grid.model';

import { drawCourt } from '../../../../d3/shots/shots';
import { drawShotStats, drawShotStatsContainer} from 'src/app/d3/shots/shot-stats';
import { plot_shots, calcShotGrid, updateShotSizes, updateShotColor, add_legend } from '../../../../d3/shots/binned-shots';
import { drawScatterLegend, drawScatterShots } from '../../../../d3/shots/scatter-shots';

@Component({
  selector: 'binned-shot-chart',
  templateUrl: './binned-shot-chart.component.html',
  styleUrls: ['./binned-shot-chart.component.css']
})
export class BinnedShotChartComponent implements OnInit {

  @Input() shots: Shot[];
  @Input() assists: Shot[];
  @Input() opponentShots: Shot[];
  @Input() isTeam: boolean;

  entityName: string;

  isBinned = true;
  isAssists = false;
  zoneAdjust = false;
  showOppShots = false;
  effMeasure = 'rel_fg';

  shotGrid: ShotGrid;
  oppShotGrid: ShotGrid;

  containerSelector = '#shot-chart-container';

  zoneAdjustTipMessage = 'Adjust shot volume against league average for each zone \n \n Most teams in the \
  league today are valuing threes and shots at the rim. This can make a lot of shot charts look similar; \
  adjusting for average volume at each zone can help enhance differences in team shot charts';

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    if (this.isTeam) {
      this.oppShotGrid = calcShotGrid(this.opponentShots, true);
      this.assists = this.shots.filter(shot => shot.assister !== null);
    }
    this.shotGrid = calcShotGrid(this.shots, this.isTeam);
    this.entityName = this.route.snapshot.paramMap.get('name');
    drawShotStatsContainer(this.containerSelector);
    drawCourt(this.containerSelector);
    this.updateChart();
  }

  updateChart() {
    if (this.isBinned) {
      const newShots = this.showOppShots ? this.opponentShots : this.shots;
      const newShotGrid = this.showOppShots ? this.oppShotGrid : this.shotGrid;

      drawShotStats(newShots, this.containerSelector);
      plot_shots(this.containerSelector, newShotGrid);
      updateShotSizes(this.zoneAdjust);
      updateShotColor(this.effMeasure, newShotGrid.zoneSideInfo);
      add_legend(this.containerSelector);
    } else {
      if (this.isAssists) {
        drawShotStats(this.assists, this.containerSelector);
        drawScatterShots(this.containerSelector, this.assists, (this.isTeam ? '!@#' : this.entityName));
      } else {
        drawShotStats(this.shots, this.containerSelector);
        drawScatterShots(this.containerSelector, this.shots, '!@#');
      }
      drawScatterLegend(this.containerSelector, false);
    }
  }

  public init(shots, opponentShots, assists) {
    this.shots = shots;
    this.opponentShots = opponentShots;
    this.shotGrid = calcShotGrid(shots, this.isTeam);
    if (this.isTeam) {
      this.oppShotGrid = calcShotGrid(opponentShots, true);
      this.assists = this.shots.filter(shot => shot.assister !== null);
    } else {
      this.assists = assists;
    }

    this.showOppShots = false;
    this.isAssists = false;
    this.isBinned = true;
    this.showOppShots = false;

    this.updateChart();
  }

}
