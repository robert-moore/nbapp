import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinnedShotChartComponent } from './binned-shot-chart.component';

describe('ShotChartComponent', () => {
  let component: BinnedShotChartComponent;
  let fixture: ComponentFixture<BinnedShotChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinnedShotChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinnedShotChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
