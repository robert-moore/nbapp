import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScatterShotChartComponent } from './scatter-shot-chart.component';

describe('ShotChartComponent', () => {
  let component: ScatterShotChartComponent;
  let fixture: ComponentFixture<ScatterShotChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScatterShotChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScatterShotChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
