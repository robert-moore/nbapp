import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { Shot } from '../../../../models/data/shot.model';
import { drawScatterShots, drawScatterLegend } from '../../../../d3/shots/scatter-shots';
import { drawCourt } from '../../../../d3/shots/shots';
import { drawShotStatsContainer, drawShotStats } from 'src/app/d3/shots/shot-stats';

@Component({
  selector: 'scatter-shot-chart',
  templateUrl: './scatter-shot-chart.component.html',
  styleUrls: ['./scatter-shot-chart.component.css']
})
export class ScatterShotChartComponent implements AfterViewInit, OnInit {

  @Input() shots: Array<Shot>;
  @Input() team: string;

  containerSelector: string;

  playerSelect: string;
  players: Array<string> = [];

  constructor() { }

  ngOnInit() {
    this.containerSelector = '#' + this.team + '-shot-chart-container';
    this.playerSelect = this.team;
  }

  ngAfterViewInit() {
    this.initializeShotCharts();
  }

  initializeShotCharts(): void {
    // filter shots by team
    this.shots = this.shots.filter(s => s.team === this.team);

    // find unique players
    this.shots.forEach(shot => {
      if (this.players.indexOf(shot.shooter) === -1) {
        this.players.push(shot.shooter);
      }
      if (this.players.indexOf(shot.assister) === -1 && shot.assister !== null) {
        this.players.push(shot.assister);
      }
    });

    // order players by number of shots + assists
    this.players.sort((a, b) => {
      const aShots = this.shots.filter(shot => (shot.shooter === a) || (shot.assister === a)).length,
            bShots = this.shots.filter(shot => (shot.shooter === b) || (shot.assister === b)).length;
      return bShots - aShots;
    });

    drawShotStatsContainer(this.containerSelector);
    drawShotStats(this.shots, this.containerSelector);

    drawCourt(this.containerSelector);
    drawScatterLegend(this.containerSelector, false);
    drawScatterShots(this.containerSelector, this.shots, this.playerSelect);
  }

  playerChange(): void {
    let newShots: Array<Shot> = [];
    let newAssists: Array<Shot> = [];

    if (this.playerSelect === this.team) {
      newShots = this.shots.filter(shot => shot.team === this.playerSelect);
      drawScatterLegend(this.containerSelector, false);
    } else {
      newShots = this.shots.filter(shot => shot.shooter === this.playerSelect);
      newAssists = this.shots.filter(shot => shot.assister === this.playerSelect);
      drawScatterLegend(this.containerSelector, true);
    }
    drawShotStats(newShots, this.containerSelector);
    newShots.push(...newAssists);
    drawScatterShots(this.containerSelector, newShots, this.playerSelect);
  }
}
