import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  scrollToFooter() {
    let footer = document.getElementById('footer');
    footer.scrollIntoView({behavior: "smooth"});
  }

}
