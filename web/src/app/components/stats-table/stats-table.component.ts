import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { PlayerGameStats } from '../../models/data/player-game-stats.model';
import { MatTableDataSource, MatSort } from '@angular/material';
import { StatsTableService } from '../../services/util/stats-table.service';
import { StatsTableToolTipMessages } from './stats-table.tooltip.messages';

@Component({
  selector: 'stats-table',
  templateUrl: './stats-table.component.html',
  styleUrls: ['./stats-table.component.css']
})
export class StatsTableComponent implements OnInit {

  @Input() team: string;
  @Input() tableType: string;
  @Input() stats: Array<PlayerGameStats>;

  dataSource = new MatTableDataSource([]);
  displayedCols: Array<string>;
  perModeDisplayed = 'totals';
  initialSortCol: string;
  initialSortDirection: string;

  tooltipMessages: StatsTableToolTipMessages;

  totalStats: Array<PlayerGameStats>;
  perGameStats: Array<PlayerGameStats>;
  perMinuteStats: Array<PlayerGameStats>;
  perPossessionStats: Array<PlayerGameStats>;
  displayStats: Array<PlayerGameStats>;

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public tableService: StatsTableService
  ) { }

  ngOnInit() {
    this.totalStats = this.stats;

    if (this.tableType !== 'player') {
      this.totalStats = this.totalStats.filter(x => x.player.team == this.team);
      this.initialSortCol = 'time';
      this.initialSortDirection = 'desc';
    } else {
      this.initialSortCol = 'season';
      this.initialSortDirection = 'asc';
    }
    this.displayStats = this.totalStats;

    this.perGameStats = this.cloneObj(this.totalStats).map(x => this.divideStatsBy(x, x.games_played));
    this.perMinuteStats = this.cloneObj(this.totalStats).map(x => this.divideStatsBy(x, x.time / 60 / 36));
    this.perPossessionStats = this.cloneObj(this.totalStats).map(x => this.divideStatsBy(x, x.possessions / 100));

    this.dataSource = new MatTableDataSource(this.displayStats);

    this.dataSource.sortingDataAccessor = (obj, property) => {
      switch (property) {
        case 'season': {
          return +obj['season'].slice(0, 4) + (obj.seasonType === "Playoffs" ? 0.5 : 0);
        };
        default: return this.getProperty(obj, property);
      }
    };
    this.dataSource.sort = this.sort;

    this.tableService.initCols();
    this.displayedCols = this.tableService.columnsDisplayed;
    this.tableService.getColums().subscribe(cols => { this.displayedCols = cols; });

    const gamesColIndex = this.displayedCols.indexOf('games');

    if (this.tableType !== 'game') {
      if (gamesColIndex === -1) { this.displayedCols.splice(2, 0, 'games'); }
    } else if (gamesColIndex > -1) {
      this.displayedCols.splice(gamesColIndex, 1);
    }

    const seasonColIndex = this.displayedCols.indexOf('season');
    if (this.tableType === 'player') {
      if (seasonColIndex === -1) { this.displayedCols.splice(0, 0, 'season'); }
    } else if (seasonColIndex > -1) {
      this.displayedCols.splice(seasonColIndex, 1);
    }

    this.tooltipMessages = new StatsTableToolTipMessages();
  }

  getProperty = (obj, path) => (path.split('.').reduce((o, p) => o && o[p], obj));

  formatMakeAttempts(makes: number, attempts: number): string {
    if (attempts === 0) {
      return '0/0 (0%)';
    } else {
      return this.round(makes, 1) + '/' + this.round(attempts, 1) + ' (' + Math.round(makes / attempts * 1000) / 10 + '%)';
    }
  }

  formatTrueShooting(makes: number, attempts: number): string {
    if (attempts === 0) {
      return '0/0 (0%)';
    } else {
      return this.round(makes, 1) + '/' + this.round(attempts, 1) + ' (' + Math.round(makes / attempts * 1000) / 20 + '%)';
    }
  }

  getTotalStat(stat: string): number {
    let sum = 0,
      divisor = 1;
    
    this.totalStats.forEach(element => { sum += this.getProperty(element, stat) });

    if (this.perModeDisplayed === 'games') {
      if (this.tableType === 'player') {
        divisor = this.totalStats.map(x => x.games_played).reduce((a, b) => a + b);
      } else if (this.tableType === 'team') {
        divisor = this.totalStats.map(x => x.time).reduce((a, b) => a + b);
        divisor = Math.floor(divisor / 60 / 48);
      }
    } else if (this.perModeDisplayed === 'minutes') {
      divisor = this.totalStats.map(x => x.time).reduce((a, b) => a + b);
      divisor = divisor / 60 / 36;
    } else if (this.perModeDisplayed === 'possessions') {
      divisor = this.totalStats.map(x => x.possessions).reduce((a, b) => a + b);
      divisor = divisor / 100;
    }

    if (this.tableType === 'team') {
      divisor = divisor / 5;
    }
    sum = sum / divisor;
    return sum;
  }

  formatTime(totalSeconds: number): string {
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = totalSeconds % 60;
    return minutes + ':' + (seconds < 10 ? '0' + this.round(seconds, 0) : this.round(seconds, 0));
  }

  updatePerMode(perMode: string) {

    if (this.perModeDisplayed !== perMode) {
      if (perMode === 'totals') {
        this.dataSource.data = this.totalStats;
      } else if (perMode === 'games') {
        this.dataSource.data = this.perGameStats;
      } else if (perMode === 'minutes') {
        this.dataSource.data = this.perMinuteStats;
      } else if (perMode === 'possessions') {
        this.dataSource.data = this.perPossessionStats;
      }

    }
    this.perModeDisplayed = perMode;
  }

  resetCols() {
    this.tableService.resetCols();
  }

  private divideStatsBy(stats: PlayerGameStats, denominator: number): PlayerGameStats {
    stats.possessions = stats.possessions / denominator;
    stats.time = stats.time / denominator;
    stats.tov = stats.tov / denominator;
    stats.plus_minus.raw = stats.plus_minus.raw / denominator;
    stats.movement.distance = stats.movement.distance / denominator;
    stats.screen_ast = stats.screen_ast / denominator;
    this.divideEachOfObject(stats.team, denominator);
    this.divideEachOfObject(stats.passing, denominator);
    this.divideEachOfObject(stats.scoring, denominator);
    this.divideEachOfObject(stats.rebounding, denominator);
    this.divideEachOfObject(stats.defense, denominator);
    this.divideEachOfObject(stats.possession, denominator);
    return stats;
  }

  private divideEachOfObject<T>(o: T, denominator: number): void {
    Object.keys(o).forEach(x => {
      if (o[x] instanceof Object) {
        this.divideEachOfObject(o[x], denominator);
      } else {
        o[x] = o[x] / denominator;
      }
    });
  }

  private round(num: number, dec: number) {
    return Math.round(num * (Math.pow(10, dec))) / (Math.pow(10, dec));
  }

  private cloneObj<T>(original: T): T {
    return JSON.parse(JSON.stringify(original));
  }

  public init(data) {
    this.totalStats = data;
    this.displayStats = this.totalStats;
    this.dataSource.data = this.displayStats;

    this.perGameStats = this.cloneObj(this.totalStats).map(x => this.divideStatsBy(x, x.games_played));
    this.perMinuteStats = this.cloneObj(this.totalStats).map(x => this.divideStatsBy(x, x.time / 60 / 36));
    this.perPossessionStats = this.cloneObj(this.totalStats).map(x => this.divideStatsBy(x, x.possessions / 100));
  }
}
