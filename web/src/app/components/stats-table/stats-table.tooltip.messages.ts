export class StatsTableToolTipMessages {
    
    player = 'Player Name';
    team = 'Player Team';
    minutes = 'Minutes played';
    trueShooting = "Points / True Shooting Attempts \n \n True Shooting Attempts = number of possessions used on a scoring attempt \n \n Field Goal attempts and trips to the free throw line increase a player's true shooting attempts \n \n 2pt and 3pt foul trips both count as 1 true shooting attempt \n \n And one's do not increase a player's true shooting attempts";
    possessions = 'Possesions Played \n \n Calculated from pace (possessions per 48 minutes and minutes played)';
    points = 'Points Scored';
    fg3 = '3 Point Shooting';
    fg2 = '2 Point Shooting';
    ft = 'Free Throw Shooting';
    ast = 'Assists';
    secondary_ast = 'Passes to a teammate who records an assist within 1 second and without dribbling';
    ft_ast = 'Passes to a teammate who draws a shooting foul within one dribble of receiving the pass';
    potential_ast = 'Any pass to a teammate who shoots within 1 dribble of receiving the ball';
    passes = 'Total passes made';
    tov = 'Turnover';
    reb = 'Rebounds / Rebound Chances \n \n Rebound Chance = any instance where a player is the closest player to the ball at any point in time between when the ball has crossed below the rim to when it is fully rebounded';
    oreb = 'Offensive Rebounds / Offensive Rebound Chances \n \n Rebound Chance = any instance where a player is the closest player to the ball at any point in time between when the ball has crossed below the rim to when it is fully rebounded';
    dreb = 'Defensive Rebounds / Defensive Rebound Chances \n \n Rebound Chance = any instance where a player is the closest player to the ball at any point in time between when the ball has crossed below the rim to when it is fully rebounded';
    box_out = 'The number of times a player made physical contact with an opponent who was actively pursuing a rebound, showed visible progress or strong effort in disadvantaging the opponent, and successfully prevented that opponent from securing the rebound';
    stl = 'Steals';
    blk = 'Blocks';
    dfg = 'Percentage of field goal attempts the opponent makes while the player was defending the rim (within 5 feet)';
    charge = 'Charges Drawn';
    deflection = 'The number of times a defensive player gets his hand on the ball on a non-shot attempt';
    recovery = 'The number of times a player gains sole possession of a live ball that is not in the control of either team';
    plus_minus = 'Team point differential with a player on the court';
    ortg = 'Team points per 100 possessions with a player on the court';
    drtg = 'Opponent points per 100 possession with a player on the court';
    screen_ast = 'The number of times a player sets a screen for a teammate that directly leads to a made field goal by that teammate';
    distance = 'Distance run';
    speed = 'Average speed';

    constructor () {}
}