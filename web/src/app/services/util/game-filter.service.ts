import { Injectable } from '@angular/core';
import { GameFilter } from '../../models/util/game-filter';

@Injectable({providedIn: 'root'})
export class GameFilterService {

  filter: GameFilter = new GameFilter();

  constructor() { 
    this.filter.season = 2018;
    this.filter.seasonType = 'Regular Season';
  }

  storeFilter(filter: GameFilter) {
    this.filter = filter;
  }

  getFilter() {
    let returnFilter = this.filter;
    this.filter = new GameFilter();
    return returnFilter;
  }
}
