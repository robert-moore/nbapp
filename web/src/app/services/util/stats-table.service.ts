import { Injectable } from '@angular/core';
import { PlayerGameStats } from '../../models/data/player-game-stats.model';
import { Observable, Subject } from 'rxjs';
import { ColumnCategory } from '../../models/util/column-category.model';

@Injectable({ providedIn: 'root' })
export class StatsTableService {

    columnCategories = {
        'season': 1,
        'player': 2,
        'games': 3,
        'time': 4,
        'possessions': 5,
        'scoring': 6,
        'passing': 7,
        'tov': 8,
        'rebounding': 9,
        'defense': 10,
        'plus_minus': 11,
        'screen_ast': 12,
        'movement': 13
    }

    columnsObs = new Subject<any>();

    columnDefs = {
        'default': new ColumnCategory(
            ['player.name', 'player.team', 'time', 'possessions', 'scoring.pts', 'scoring.ts_pct',
                'passing.ast', 'tov', 'rebounding.reb', 'defense.blk', 'defense.stl', 'plus_minus.raw']
        ),
        'scoring': new ColumnCategory(['fg2m', 'fg3m', 'ftm']),
        'passing': new ColumnCategory(['potential_ast', 'ft_ast', 'secondary_ast', 'passes']),
        'rebounding': new ColumnCategory(['oreb', 'dreb', 'box_outs']),
        'defense': new ColumnCategory(['rim_protection', 'charges_drawn', 'deflections', 'recoveries']),
        'plus_minus': new ColumnCategory(['ortg', 'drtg']),
        'misc': new ColumnCategory(['screen_ast', 'distance', 'speed'])
    }

    columnsDisplayed = [];
    dataSource: Array<PlayerGameStats> = [];

    initCols() {
        this.columnsDisplayed = this.columnDefs['default'].columns;
    }

    resetCols() {
        this.columnsDisplayed = this.columnDefs['default'].columns;
        this.columnsObs.next(this.columnsDisplayed);
    }

    toggleColumns(toggleCols: string): void {
        this.columnDefs[toggleCols].isDisplayed = !this.columnDefs[toggleCols].isDisplayed;

        this.columnsDisplayed = this.cloneObj(this.columnDefs['default'].columns);

        Object.keys(this.columnDefs).forEach(colType => {
            if (this.columnDefs[colType].isDisplayed) {
                const insertIndex = this.getIndexOfLastOfType(colType, this.columnsDisplayed);
                const colsToAdd = [];
                this.columnDefs[colType].columns.forEach(col => colsToAdd.push(colType + '.' + col));
                this.columnsDisplayed.splice(insertIndex, 0, ...colsToAdd);
            }
        });
        this.columnsObs.next(this.columnsDisplayed);
    }

    getColums(): Observable<Array<string>> {
        return this.columnsObs.asObservable();
    }

    private getIndexOfLastOfType(type: string, columns: Array<string>) {
        let index = -1;
        for (let i = 0; i < columns.length; i++) {
            if (columns[i].includes(type)) {
                index = i;
            }
        }
        return index > 0 ? index + 1 : columns.length;
    }

    private cloneObj<T>(original: T): T {
        return JSON.parse(JSON.stringify(original));
    }
}
