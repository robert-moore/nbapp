import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { GameSummary } from '../../models/data/game-summary.model';
import { GameDetails } from '../../models/data/game-details.model';

import { environment } from '../../../environments/environment';

import { BaseService } from './base.service';

@Injectable({ providedIn: 'root' })
export class GameService extends BaseService {

  private gamesUrl = environment.api + 'games';
  private gameDetailUrl = environment.api + 'gameDetail';

  constructor(
    http: HttpClient,
  ) {
    super(http)
  }

  getGames(): Observable<GameSummary[]> {
    return this.http.get<GameSummary[]>(this.gamesUrl).pipe(
      catchError(this.handleError('getGames', []))
    );
  }

  getGame(id: number): Observable<GameDetails> {
    const url = `${this.gameDetailUrl}/${id}`;
    return this.http.get<GameDetails>(url).pipe(
      catchError(this.handleError<GameDetails>(`getGame id=${id}`)
      )
    );
  }

}
