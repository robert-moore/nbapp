import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';

@Injectable({ providedIn: 'root' })
export class PlayerService extends BaseService {

    private playersUrl = environment.api + 'players';
    private playerDetailUrl = environment.api + 'playerDetail';
    private playerConsistencyUrl = environment.api + 'players/consistency';

    constructor(
        http: HttpClient
    ) {
        super(http);
    }

    public getPlayers(): Observable<string[]> {
        return this.http.get<string[]>(this.playersUrl).pipe(
            catchError(this.handleError('getPlayers', []))
        );
    }

    getPlayer(player: string, season: string, seasonType: string): Observable<any> {
        return this.http.post<any>(
            this.playerDetailUrl,
            {
                'player': player,
                'season': season,
                'seasonType': seasonType
            }).pipe(
                catchError(this.handleError('getPlayerDetail', []))
            );
    }

    getPlayerConsistency(season: number, seasonType: string, team: string, numPlayer: number, stat: string): Observable<any> {
        return this.http.post<any>(
            this.playerConsistencyUrl,
            {
                'season': season,
                'seasonType': seasonType,
                'numPlayers': numPlayer,
                'team': team,
                'stat': stat
            }).pipe(
                catchError(this.handleError('getPlayerConsistency', []))
            );
    }
}
