import { HttpClient } from '@angular/common/http';
import { environment } from "../../../environments/environment";
import { Observable, of } from 'rxjs';

export class BaseService {

    protected apiUrl = environment.api;

    constructor(
        protected http: HttpClient, 
    ) { }

    protected handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
