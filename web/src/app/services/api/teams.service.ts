import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Team } from '../../models/data/team.model';
import { catchError, tap } from 'rxjs/operators';
import { environment } from "../../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "./base.service";
import { TeamDetail } from "../../models/data/team-detail.model";

@Injectable({ providedIn: 'root' })
export class TeamService extends BaseService {

    private teamsUrl = environment.api + 'teams';
    private teamDetailUrl = environment.api + 'teamDetail';

    constructor(
        http: HttpClient
    ) { 
        super(http)
    }

    public getTeams(): Observable<Team[]> {
        return this.http.get<Team[]>(this.teamsUrl).pipe(
            catchError(this.handleError('getTeams', []))
        );
    }

    getTeam(team: string, season: string, seasonType: string): Observable<TeamDetail> {
        return this.http.post<any>(this.teamDetailUrl, {'team': team, 'season': season, 'seasonType': seasonType}).pipe(
            catchError(this.handleError('getTeamDetail', []))
        )
    }
}