import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';

import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

import { GameSummary } from '../../../models/data/game-summary.model';
import { GameFilter } from '../../../models/util/game-filter';
import { GameService } from '../../../services/api/game.service';
import { GameFilterService } from '../../../services/util/game-filter.service';
import { SEASONS, SEASON_TYPES } from '../../../enums';
import { Team } from '../../../models/data/team.model';
import { TeamService } from '../../../services/api/teams.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit, OnDestroy {

  filter: GameFilter = new GameFilter();

  minDate = new Date(2016, 10, 24);
  maxDate = new Date(2018, 5, 8);

  teams: Team[];
  seasons = SEASONS;
  seasonTypes = SEASON_TYPES;

  allGames: GameSummary[];

  displayedCols = ['home.team', 'away.team', 'date', 'season', 'season_type', 'game_page'];
  dataSource = new MatTableDataSource([]);
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private gameService: GameService, 
    private filterSerivce: GameFilterService,
    private teamService: TeamService
  ) { }

  ngOnInit() {
    this.getGames();
    this.getTeams();
    this.filter = this.filterSerivce.getFilter();
  }

  ngOnDestroy() {
    this.filterSerivce.storeFilter(this.filter);
  }

  getGames(): void {
    this.gameService.getGames().subscribe(games => {
      this.allGames = games;
      this.filterChange();
      this.dataSource.sortingDataAccessor = (obj, property) => {
        switch (property) {
          case 'date': return new Date(obj.date);
          default: return this.getProperty(obj, property);
        }
      };
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  getTeams(): void {
    this.teamService.getTeams().subscribe(teams => this.teams = teams);
  }

  filterChange() {
    let newGames = this.allGames;

    if (this.filter.date) { newGames = newGames.filter(game => this.convertDate(new Date(game.date)) + 86400000 == this.convertDate(this.filter.date)); }
    if (this.filter.team1) { newGames = newGames.filter(game => this.teamInGame(game, this.filter.team1)); }
    if (this.filter.team2) { newGames = newGames.filter(game => this.teamInGame(game, this.filter.team2)); }
    if (this.filter.season) { newGames = newGames.filter(game => game.season == this.filter.season); }
    if (this.filter.seasonType) { newGames = newGames.filter(game => game.season_type == this.filter.seasonType); }
    this.dataSource.data = newGames;
  }

  clearFilters() {
    this.filter = new GameFilter();
    this.filterChange();
  }

  teamInGame(game: GameSummary, team: String) {
    return game.home.team == team || game.away.team == team;
  }

  convertDate(date: Date): number {
    date.setHours(0, 0, 0);
    return date.getTime()
  }

  getProperty = (obj, path) => (path.split('.').reduce((o, p) => o && o[p], obj));

}
