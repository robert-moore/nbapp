import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { TeamService } from 'src/app/services/api/teams.service';
import { SEASON_TYPES, SEASONS } from 'src/app/enums';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  displayedCols = ['name', 'teamPage'];
  dataSource = new MatTableDataSource([]);
  season = 2018;
  seasonType = 'Regular Season';
  seasonTypes = SEASON_TYPES;
  seasons = SEASONS;

  constructor(
    private teamService: TeamService
  ) { }

  ngOnInit() {
    this.teamService.getTeams().subscribe(teams => {
      this.dataSource = new MatTableDataSource(teams);
    });
  }

}
