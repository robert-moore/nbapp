import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamService } from '../../../services/api/teams.service';
import { SEASON_TYPES, SEASONS } from '../../../enums';
import { SeasonRotationsVizComponent } from '../../../components/viz/rotations/season-rotations-viz/season-rotations-viz.component';
import { TeamDetail } from '../../../models/data/team-detail.model';
import { BinnedShotChartComponent } from '../../../components/viz/shots/binned-shot-chart/binned-shot-chart.component';
import { StatsTableComponent } from '../../../components/stats-table/stats-table.component';
import { PlayTypesComponent } from '../../../components/viz/play-type/play-types/play-types.component';

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit {

  seasonTypes = SEASON_TYPES;
  seasons = SEASONS;

  team: string;

  currentSeason: number;
  currentSeasonType: string;

  season: number;
  seasonType: string;

  teamDetails: TeamDetail;

  @ViewChild('rotations') private rotations: SeasonRotationsVizComponent;
  @ViewChild('shots') private shots: BinnedShotChartComponent;
  @ViewChild('stats') private stats: StatsTableComponent;
  @ViewChild('playTypes') private playTypes: PlayTypesComponent;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private teamService: TeamService
  ) { }

  ngOnInit() {

    const params = this.route.snapshot.paramMap;
    this.team = params.get('name');

    this.season = +params.get('season');
    this.currentSeason = this.season;

    this.seasonType = params.get('seasonType');
    this.currentSeasonType = this.seasonType;

    this.teamService.getTeam(this.team, this.season.toString(), this.seasonType)
      .subscribe(teamDetails => this.teamDetails = teamDetails);
  }

  routeToSelection() {
    if (this.season !== this.currentSeason || this.seasonType !== this.currentSeasonType) {
      this.router.navigate(['/teamDetail/', this.team, this.season, this.seasonType], { relativeTo: this.route, skipLocationChange: true, replaceUrl: true });

      this.teamService.getTeam(this.team, this.season.toString(), this.seasonType)
        .subscribe(teamDetails => {
          this.teamDetails = teamDetails;

          if (this.rotations) {
            this.rotations.init(teamDetails.rotations);
          }
          if (this.shots) {
            this.shots.init(teamDetails.shots, teamDetails.oppShots, null);
          }
          if (this.stats) {
            this.stats.init(teamDetails.stats);
          }
          if (this.playTypes) {
            this.playTypes.init(teamDetails.playType);
          }
          this.currentSeason = this.season;
          this.currentSeasonType = this.seasonType;
        });
    }
  }

}
