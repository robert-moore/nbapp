import { Component, OnInit } from '@angular/core';
import { PlayerService } from 'src/app/services/api/players.service';
import { plotConsistency, clearChart } from 'src/app/d3/consistnecy';
import { SEASONS, SEASON_TYPES } from 'src/app/enums';
import { TeamService } from 'src/app/services/api/teams.service';

@Component({
  selector: 'consistency',
  templateUrl: './consistency.component.html',
  styleUrls: ['./consistency.component.css']
})
export class ConsistencyComponent implements OnInit {

  seasons = SEASONS;
  seasonTypes = SEASON_TYPES;
  teams = [];
  numPlayerOptions = [5, 10, 15, 20];
  statOptions = [
    {'display': 'Points', 'path': 'scoring.pts'},
    {'display': 'True Shooting Attempts', 'path': 'scoring.tsa'},
    {'display': 'Assists', 'path': 'passing.ast'},
    {'display': 'Potential Assists', 'path': 'passing.potential_ast'},
    {'display': 'Assist Points', 'path': 'passing.ast_pts'},
    // {'display': 'Plus Minus', 'path': 'plus_minus.raw'}
  ];

  isLoading = true;
  disableSeasonType = true;

  season = 2018;
  team = '';
  seasonType = 'Regular Season';
  numPlayers = 10;
  stat = this.statOptions[0];

  constructor(
    private playerService: PlayerService,
    private teamService: TeamService
  ) { }

  ngOnInit() {
    this.teamService.getTeams().subscribe(teams => this.teams = teams);
    this.getData();
  }

  getData() {
    clearChart();
    this.isLoading = true;
    this.playerService.getPlayerConsistency(this.season, this.seasonType, this.team, this.numPlayers, this.stat.path)
        .subscribe(data => {
          plotConsistency(data);
          this.isLoading = false;
        });
  }

  updateSeason() {
    this.disableSeasonType = this.season === 2018;
    if (this.season === 2018) { this.seasonType = 'Regular Season'; }
  }

}
