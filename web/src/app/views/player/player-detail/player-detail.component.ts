import { Component, OnInit, ViewChild } from '@angular/core';
import { SEASONS, SEASON_TYPES } from '../../../enums';
import { Shot } from '../../../models/data/shot.model';
import { Router, ActivatedRoute } from '@angular/router';
import { PlayerService } from '../../../services/api/players.service';
import { BinnedShotChartComponent } from '../../../components/viz/shots/binned-shot-chart/binned-shot-chart.component';
import { PlayerGameStats } from '../../../models/data/player-game-stats.model';
import { PlayTypesComponent } from '../../../components/viz/play-type/play-types/play-types.component';
import { StatsTableComponent } from '../../../components/stats-table/stats-table.component';

@Component({
  selector: 'app-player-detail',
  templateUrl: './player-detail.component.html',
  styleUrls: ['./player-detail.component.css']
})
export class PlayerDetailComponent implements OnInit {

  @ViewChild('shotsComp') private shotsComponent: BinnedShotChartComponent;
  @ViewChild('playTypesComp') private playTypesComponent: PlayTypesComponent;
  @ViewChild('statsTableComp') private statsTableComponent: StatsTableComponent;

  playerName: string;

  seasons = SEASONS;
  season: number;
  currentSeason: number;

  seasonTypes = SEASON_TYPES;
  seasonType: string;
  currentSeasonType: string;

  shots: Shot[];
  assists: Shot[];
  playTypeData: any[];
  statsData: PlayerGameStats[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private playerService: PlayerService
  ) { }

  ngOnInit() {
    const params = this.route.snapshot.paramMap;

    this.playerName = params.get('name');
    this.season = +params.get('season');
    this.seasonType = params.get('seasonType');

    this.currentSeason = this.season;
    this.currentSeasonType = this.seasonType;

    this.playerService.getPlayer(this.playerName, this.season.toString(), this.seasonType).subscribe(data => {
      this.shots = data.shots.filter(s => s.shooter === this.playerName);
      this.assists = data.shots.filter(s => s.assister === this.playerName);
      this.playTypeData = data.playType;
      this.statsData = data.stats;
    })
  }

  routeToSelection() {
    if (this.season !== this.currentSeason || this.seasonType !== this.currentSeasonType) {
      this.router.navigate(
        ['/playerDetail/', this.playerName, this.season, this.seasonType],
        { relativeTo: this.route, skipLocationChange: true, replaceUrl: true }
      );

      this.playerService.getPlayer(this.playerName, this.season.toString(), this.seasonType).subscribe(data => {
        this.shots = data.shots.filter(s => s.shooter === this.playerName);
        this.assists = data.shots.filter(s => s.assister === this.playerName);
        this.statsData = data.stats;

        if (this.shotsComponent) { this.shotsComponent.init(this.shots, null, this.assists); }
        if (this.playTypesComponent) { this.playTypesComponent.init(this.playTypeData); }
        if (this.statsTableComponent) { this.statsTableComponent.init(this.statsData); }
      });

      this.currentSeason = this.season;
      this.currentSeasonType = this.seasonType;
    }
  }

}
