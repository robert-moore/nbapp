import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { PlayerService } from 'src/app/services/api/players.service';
import { SEASONS, SEASON_TYPES } from 'src/app/enums';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  season = 2018;
  seasons = SEASONS;

  seasonType = 'Regular Season';
  seasonTypes = SEASON_TYPES;

  displayedCols = ['name', 'playerPage'];
  dataSource = new MatTableDataSource([]);

  constructor(
    private playerService: PlayerService
  ) { }

  ngOnInit() {

    this.playerService.getPlayers().subscribe(players => {
      this.dataSource = new MatTableDataSource(players);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

}
