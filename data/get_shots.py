from util.format import format_year_string, format_game_id
from util.data import api_data_dir
from helpers.shots import merge_shot_pbp_for_game


def get_shots_for_game(season, season_type, game_id):
    df = merge_shot_pbp_for_game(season, season_type, game_id, True)

    df = df[['GAME_ID', 'PLAYER1_NAME', 'PLAYER2_NAME', 'PLAYER1_TEAM_ABBREVIATION', 'LOC_X', 'LOC_Y', 'SHOT_MADE_FLAG', 'SHOT_TYPE', 'SHOT_ZONE_BASIC', 'SHOT_ZONE_AREA']]
    df = df.rename(columns={
        'GAME_ID': 'game_id',
        'PLAYER1_NAME': 'shooter',
        'PLAYER2_NAME': 'assister',
        'PLAYER1_TEAM_ABBREVIATION': 'team',
        'LOC_X': 'x',
        'LOC_Y': 'y',
        'SHOT_MADE_FLAG': 'made',
        'SHOT_TYPE': 'value',
        'SHOT_ZONE_BASIC': 'zone',
        'SHOT_ZONE_AREA': 'side'
    })

    zone_map = {
        'Restricted Area': 'RA',
        'In The Paint (Non-RA)': 'ShortMid',
        'Mid-Range': 'Mid',
        'Left Corner 3': 'Corner3',
        'Right Corner 3': 'Corner3',
        'Above the Break 3': 'Top3',
        'Backcourt': 'Heave'
    }

    df['x'] = df['x'].astype(int)
    df['y'] = df['y'].astype(int)
    df['made'] = df['made'].apply(lambda x: int(x) == 1)
    df['value'] = df['value'].apply(lambda x: 2 if '2PT' in x else 3)
    df['zone'] = df['zone'].apply(lambda x: zone_map[x])
    df['side'] = df['side'].apply(lambda x: x.split('(')[1].split(')')[0])
    df['game_id'] = df['game_id'].apply(lambda x: format_game_id(x))

    return df
