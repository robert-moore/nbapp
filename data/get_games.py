import pandas as pd

from scrappers import TeamAdvancedGameLogs
from util.format import format_game_id


def get_game_ids(season, season_type):
    game_log_ep = TeamAdvancedGameLogs()
    game_log_df = game_log_ep.get_data({'Season': season, 'SeasonType': season_type}, override_file=True)
    if len(game_log_df) > 0:
        return game_log_df['GAME_ID'].unique()
    else:
        return []


def get_game_objects(season, season_type, game_ids):
    game_log_ep = TeamAdvancedGameLogs()
    df = game_log_ep.get_data({'Season': season, 'SeasonType': season_type})

    formatted_data = []
    for gid in game_ids:
            game_df = df[df['GAME_ID'] == gid]
            
            [home_index, away_index] = [0, 1] if 'vs.' in game_df.iloc[0]['MATCHUP'] else [1, 0]

            formatted_data.append(dict(
                home_team=game_df.iloc[home_index]['TEAM_ABBREVIATION'],
                away_team=game_df.iloc[away_index]['TEAM_ABBREVIATION'],
                home_score=game_df.iloc[home_index]['PTS'],
                away_score=game_df.iloc[away_index]['PTS'],
                date=game_df.iloc[0]['GAME_DATE'].split('T')[0],
                id=format_game_id(game_df.iloc[0]['GAME_ID']),
                season=season.split('-')[0],
                season_type=season_type
            ))

    return pd.DataFrame(formatted_data)
