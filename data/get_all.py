import pandas as pd

from util.data import api_data_dir, api_hist_data_dir
from util.format import format_game_id

from get_games import get_game_ids, get_game_objects
from get_rotations import get_rotations_for_game, get_scores_for_game
from get_shots import get_shots_for_game
from get_stats import get_stats_for_game

from config import current_season, current_season_type

game_ids_for_current_season = get_game_ids(current_season, current_season_type)
historical_game_ids = pd.read_csv(api_hist_data_dir + 'games.csv')['id'].unique()
new_game_ids = [game_id for game_id in game_ids_for_current_season if game_id not in historical_game_ids]

if len(new_game_ids) > 0:

    game_objects = get_game_objects(current_season, current_season_type, new_game_ids)
    rotation_objects = pd.DataFrame()
    score_objects = pd.DataFrame()
    shot_objects = pd.DataFrame()
    stat_objects = pd.DataFrame()

    new_game_ids = [format_game_id(game_id) for game_id in new_game_ids]
    games_to_add = []

    for game_id in new_game_ids:

        try:
            game_rotations = get_rotations_for_game(current_season, current_season_type, game_id)
            game_scores = get_scores_for_game(current_season, current_season_type, game_id)
            game_shots = get_shots_for_game(current_season, current_season_type, game_id)
            game_stats = get_stats_for_game(current_season, current_season_type, game_id, True)
            games_to_add.append(game_id)

            rotation_objects = rotation_objects.append(game_rotations)
            score_objects = score_objects.append(game_scores)
            shot_objects = shot_objects.append(game_shots)
            stat_objects = stat_objects.append(game_stats)

        except Exception as e:
            print(e)
            continue

    game_objects = game_objects[game_objects['id'].isin(games_to_add)]

    pd.DataFrame(game_objects).to_csv(api_data_dir + 'games.csv', index=False)
    pd.DataFrame(rotation_objects).to_csv(api_data_dir + 'rotations.csv', index=False)
    pd.DataFrame(score_objects).to_csv(api_data_dir + 'score.csv', index=False)
    pd.DataFrame(shot_objects).to_csv(api_data_dir + 'shots.csv', index=False)
    pd.DataFrame(stat_objects).to_csv(api_data_dir + 'stats.csv', index=False)

    pd.read_csv(api_hist_data_dir + 'games.csv').append(game_objects).to_csv(api_hist_data_dir + 'games.csv', index=False)
    pd.read_csv(api_hist_data_dir + 'rotations.csv').append(rotation_objects).to_csv(api_hist_data_dir + 'rotations.csv', index=False)
    pd.read_csv(api_hist_data_dir + 'score.csv').append(score_objects).to_csv(api_hist_data_dir + 'score.csv', index=False)
    pd.read_csv(api_hist_data_dir + 'shots.csv').append(shot_objects).to_csv(api_hist_data_dir + 'shots.csv', index=False)
    pd.read_csv(api_hist_data_dir + 'stats.csv').append(stat_objects).to_csv(api_hist_data_dir + 'stats.csv', index=False)

else:
    print('No New Games!')
    pd.DataFrame().to_csv(api_data_dir + 'games.csv', index=False)
    pd.DataFrame().to_csv(api_data_dir + 'rotations.csv', index=False)
    pd.DataFrame().to_csv(api_data_dir + 'score.csv', index=False)
    pd.DataFrame().to_csv(api_data_dir + 'shots.csv', index=False)
    pd.DataFrame().to_csv(api_data_dir + 'stats.csv', index=False)