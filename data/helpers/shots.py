import pandas as pd

from util.format import format_game_id
from scrappers.PlayByPlay import PlayByPlay
from scrappers.Shots import ShotChartDetail
from scrappers.GameLog import TeamAdvancedGameLogs


def merge_shot_pbp_for_season(season, season_type='Regular Season'):
    play_by_play_endpoint = PlayByPlay()
    shot_endpoint = ShotChartDetail()

    pbp_df = pd.DataFrame()
    log = TeamAdvancedGameLogs().get_data({'Season': season, 'SeasonType': season_type})
    games = log.GAME_ID.unique()
    for g in games:
        g = format_game_id(g)
        pbp_df = pbp_df.append(
            play_by_play_endpoint.get_data({'GameID': g, 'Season': season, 'SeasonType': season_type}))

    pbp_df['GAME_ID'] = pbp_df['GAME_ID'].apply(lambda x: format_game_id(x))
    shots_df = shot_endpoint.get_data({'Season': season, 'SeasonType': season_type}, override_file=True)

    pbp_df['EVENTNUM'] = pbp_df['EVENTNUM'].astype(int)
    pbp_df['GAME_ID'] = pbp_df['GAME_ID'].astype(int)
    pbp_df['PERIOD'] = pbp_df['PERIOD'].astype(int)

    shots_df['GAME_EVENT_ID'] = shots_df['GAME_EVENT_ID'].astype(int)
    shots_df['GAME_ID'] = shots_df['GAME_ID'].astype(int)
    shots_df['PERIOD'] = shots_df['PERIOD'].astype(int)

    merge_df = pd.merge(pbp_df, shots_df, left_on=['EVENTNUM', 'GAME_ID', 'PERIOD'], right_on=['GAME_EVENT_ID', 'GAME_ID', 'PERIOD'], how='inner')
    return merge_df


def merge_shot_pbp_for_game(season, season_type, game_id, only_fga):
    pbp_ep = PlayByPlay()

    pbp_df = pbp_ep.get_data({
        'GameID': game_id, 
        'Season': season, 
        'SeasonType': season_type
    })

    shot_endpoint = ShotChartDetail()
    shots_df = shot_endpoint.get_data({'Season': season, 'SeasonType': season_type, 'GameID': game_id})
    merge_df = pd.merge(pbp_df, shots_df, left_on=['GAME_ID', 'EVENTNUM', 'PERIOD'], right_on=['GAME_ID', 'GAME_EVENT_ID', 'PERIOD'], how='outer')

    if (only_fga):
        merge_df = merge_df[merge_df['SHOT_ATTEMPTED_FLAG'] == 1]
    
    return merge_df
