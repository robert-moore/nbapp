import pandas as pd
from requests import ConnectionError
import logging
import sys

from scrappers.BoxScore import BoxScoreTraditional, BoxScoreAdvanced, BoxScoreHustle, BoxScoreTracking
from scrappers.Tracking import TrackingStats
from shots import merge_shot_pbp_for_game
from util.pbp_parsers import is_home

logging.basicConfig(stream=sys.stdout, level=logging.INFO)


def get_box_score_player_stats(game_id, season_type='Regular Season', override_file=False):
    traditional = BoxScoreTraditional().get_data({'GameID': game_id, 'SeasonType': season_type},
                                                 override_file=override_file)
    advanced = BoxScoreAdvanced().get_data(
        {'GameID': game_id, 'SeasonType': season_type}, override_file=override_file)
    
    try:
        hustle = BoxScoreHustle().get_data(
            {'GameID': game_id}, override_file=override_file)
    except ConnectionError as e:
        logging.info('Hustle Stats not available for ' + game_id)
        hustle = pd.DataFrame()
        hustle['PLAYER_ID'] = advanced['PLAYER_ID']
        hustle['TEAM_ID'] = advanced['TEAM_ID']
        hustle['BOX_OUTS'] = 0
        hustle['DEFLECTIONS'] = 0
        hustle['LOOSE_BALLS_RECOVERED'] = 0
        hustle['CHARGES_DRAWN'] = 0
        hustle['CONTESTED_SHOTS_2PT'] = 0
        hustle['CONTESTED_SHOTS_3PT'] = 0
        hustle['SCREEN_ASSISTS'] = 0

    tracking = BoxScoreTracking().get_data(
        {'GameID': game_id, 'SeasonType': season_type}, override_file=override_file)

    merge_columns = ['TEAM_ID', 'PLAYER_ID']

    merge_df = pd.merge(traditional, advanced, on=merge_columns,
                        suffixes=('', 'adv'), how='outer')
    
    merge_df = pd.merge(merge_df, tracking, on=merge_columns,
                        suffixes=('', 'track'), how='outer')

    merge_df = pd.merge(merge_df, hustle, on=merge_columns,
                        suffixes=('', 'hustle'), how='outer')

    return merge_df


def get_stats_from_pbp(pbp_df, game_id, season, season_type='Regular Season'):
    pbp_df = merge_shot_pbp_for_game(season, season_type, game_id, False)
    players = pbp_df['PLAYER1_NAME'].unique()[1:]

    player_data = []
    for p in players:
        player_df = pbp_df[pbp_df['PLAYER1_NAME'] == p]

        try:
            desc = 'HOMEDESCRIPTION' if is_home(player_df,
                                                player_df['PLAYER1_TEAM_ABBREVIATION'].iloc[
                                                    0]) else 'VISITORDESCRIPTION'
        except IndexError as e:
            logging.warn("{} not found in game: {} {} {}".format(
                p, game_id, season, season_type))
            continue

        ft_df = player_df[player_df['EVENTMSGTYPE'] == 3]
        ft1a = len(ft_df[ft_df[desc].str.contains('of 1')])
        ft2a = len(ft_df[ft_df[desc].str.contains('of 2')])
        ft3a = len(ft_df[ft_df[desc].str.contains('of 3')])
        ft_tech = len(ft_df[ft_df[desc].str.contains('Free Throw Technical')])
        ftm = len(ft_df[ft_df[desc].str.contains('PTS')])

        fg2_df = player_df[player_df['SHOT_TYPE'] == '2PT Field Goal']
        fg2a = len(fg2_df)
        fg2m = len(fg2_df[fg2_df['SHOT_MADE_FLAG'] == 1])

        fg3_df = player_df[player_df['SHOT_TYPE'] == '3PT Field Goal']
        fg3a = len(fg3_df)
        fg3m = len(fg3_df[fg3_df['SHOT_MADE_FLAG'] == 1])

        pts = (3 * fg3m) + (2 * fg2m) + ftm
        tsa = (ft2a / 2) + (ft3a / 3) + fg2a + fg3a
        eff = pts / (2 * tsa) if tsa != 0 else 0

        assist_df = pbp_df[(pbp_df['PLAYER2_NAME'] == p) &
                           (pbp_df['SHOT_ATTEMPTED_FLAG'] == 1)]
        ast_pts = ((len(assist_df[assist_df['SHOT_TYPE'] == '3PT Field Goal'])) * 3) + (
            (len(assist_df[assist_df['SHOT_TYPE'] == '2PT Field Goal'])) * 2)

        p_data = {
            'PLAYER_NAME': p,
            'AND_ONE': ft1a,
            '2PT_FTA': ft2a,
            '3PT_FTA': ft3a,
            'TECH_FTA': ft_tech,
            '2PT_FGA': fg2a,
            '2PT_FGM': fg2m,
            '3PT_FGA': fg3a,
            '3PT_FGM': fg3m,
            'TSA': tsa,
            'EFF': round(eff * 100, 2),
            'AST_PTS': ast_pts,
        }
        player_data.append(p_data)

    return pd.DataFrame(player_data)


def get_tracking_stats_for_date(game_date, year, season_type='Regular_Season', data_override=False):
    tracking_ep = TrackingStats()

    all_cols = []
    passing_cols = ['POTENTIAL_AST']
    poss_cols = ['TIME_OF_POSS', 'ELBOW_TOUCHES',
                 'POST_TOUCHES', 'PAINT_TOUCHES']

    all_cols.extend(passing_cols)
    all_cols.extend(poss_cols)

    try:
        passing_df = tracking_ep.get_data({'PtMeasureType': 'Passing', 'DateFrom': game_date, 'DateTo': game_date,
                                           'Season': year, 'SeasonType': season_type},
                                          override_file=data_override)[['PLAYER_NAME'] + passing_cols]
    except KeyError:
        logging.error("Tracking Passings stats are not available for data: {} season: {} season type: {}".format(
            game_date, year, season_type))
        passing_df = pd.DataFrame()
        passing_df['PLAYER_NAME'] = ""

    try:
        possession_df = tracking_ep.get_data({'PtMeasureType': 'Possessions', 'DateFrom': game_date,
                                              'DateTo': game_date, 'Season': year, 'SeasonType': season_type},
                                             override_file=data_override)[['PLAYER_NAME'] + poss_cols]
    except KeyError:
        logging.error("Tracking Possession stats are not available for data: {} season: {} season type: {}".format(
            game_date, year, season_type))
        possession_df = pd.DataFrame()
        possession_df['PLAYER_NAME'] = ""

    merge_df = pd.merge(passing_df, possession_df, on='PLAYER_NAME', how='outer')

    return merge_df


def get_hustle_stats_for_data(game_date, year, data_override=False):
    hustle_ep = HustleStats()
    hustle_df = hustle_ep.get_data({
        'DateFrom': game_date,
        'DateTo': game_date,
        'Season': year
    }, override_file=data_override)[
        ['PLAYER_NAME', 'TEAM_ABBREVIATION', 'MIN', 'CONTESTED_SHOTS_2PT', 'CONTESTED_SHOTS_3PT', 'CHARGES_DRAWN',
         'DEFLECTIONS', 'LOOSE_BALLS_RECOVERED', 'SCREEN_ASSISTS', 'BOX_OUTS']]
    return hustle_df
