from scrappers import PlayByPlay
from helpers.rotations import get_rotation_data_for_game, get_score_data_for_game


def get_rotations_for_game(season, season_type, game_id):
    pbp_ep = PlayByPlay()

    pbp_df = pbp_ep.get_data({
        'GameID': game_id, 
        'Season': season, 
        'SeasonType': season_type
    })

    try:
        game_rotation_df = get_rotation_data_for_game(pbp_df)
    except ValueError as e:
        raise ValueError(e)

    game_rotation_df = game_rotation_df[['player', 'team', 'start_time', 'end_time']]
    game_rotation_df['game_id'] = game_id
    return game_rotation_df


def get_scores_for_game(season, season_type, game_id):
    pbp_ep = PlayByPlay()
    
    pbp_df = pbp_ep.get_data({
        'GameID': game_id, 
        'Season': season, 
        'SeasonType': season_type
    })

    try:
        game_score_df = get_score_data_for_game(pbp_df)
        game_score_df['game_id'] = game_id
    except ValueError as e:
        raise ValueError(e)
    
    return game_score_df
