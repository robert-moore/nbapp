import pandas as pd
import logging
import sys

from scrappers.StatsEndpoint import param_list
from config import season_types
from scrappers.PlayType import SynergyTeamStats, SynergyPlayerStats
from util.data import api_data_dir

logging.basicConfig(stream=sys.stdout, level=logging.INFO)


play_types = param_list['category']['choices']
season_type_map = {
    'Regular Season': 'Reg',
    'Playoffs': 'Post'
}
seasons = range(2018, 2019)
team_ep = SynergyTeamStats()
player_ep = SynergyPlayerStats()

database_df = pd.DataFrame()
for season in seasons:
    for season_type in season_types:
        if season_type in season_type_map.keys():
            for play_type in play_types:
                team_df = team_ep.get_data(
                    {'category': play_type, 'season': str(season), 'seasonType': season_type_map[season_type]}, override_file=True)
                player_df = player_ep.get_data(
                    {'category': play_type, 'season': str(season), 'seasonType': season_type_map[season_type]}, override_file=True)

                team_df['playType'] = play_type
                player_df['playType'] = play_type

                team_df['isTeam'] = True
                player_df['isTeam'] = False

                try:
                    team_df = team_df[['playType', 'TeamNameAbbreviation',
                                    'Time', 'PPP', 'season', 'seasonType', 'isTeam', 'Poss', 'PossG']]

                    team_df = team_df.rename(columns={
                        'TeamNameAbbreviation': 'name',
                        'PPP': 'ppp',
                        'Time': 'freq',
                        'PossG': 'possG',
                        'Poss': 'poss'
                    })

                    database_df = database_df.append(team_df)
                except KeyError as e:
                    logging.error(e)

                try:
                    player_df['name'] = player_df['PlayerFirstName'] + \
                        ' ' + player_df['PlayerLastName']

                    player_df = player_df[[
                        'playType', 'name', 'Time', 'PPP', 'season', 'seasonType', 'isTeam', 'Poss', 'PossG']]

                    player_df = player_df.rename(columns={
                        'PPP': 'ppp',
                        'Time': 'freq',
                        'Poss': 'poss',
                        'PossG': 'possG'
                    })

                    database_df = database_df.append(player_df)
                except KeyError as e:
                    logging.error(e)

database_df.to_csv(api_data_dir + 'playType.csv', index=False)
