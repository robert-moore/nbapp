from scrappers.StatsEndpoint import EndPoint, default_year
from scrappers.GameLog import TeamAdvancedGameLogs
from util.data import raw_data_dir


class PlayByPlay(EndPoint):
    base_url = 'http://stats.nba.com/stats/playbyplayv2'
    default_params = {
        'EndPeriod': '10',
        'EndRange': '55800',
        'GameID': '0021700216',
        'RangeType': '2',
        'Season': default_year,
        'SeasonType': 'Regular Season',
        'StartPeriod': '1',
        'StartRange': '0'
    }

    def determine_file_path(self, params):
        return raw_data_dir + 'playbyplayv2/' + str(params['Season']) + '/' + str(params['GameID']) + '.csv'

    def update_data(self, season=default_year, season_type='Regular Season'):
        log = TeamAdvancedGameLogs().get_data(
            {'Season': season, 'SeasonType': season_type},
            override_file=True
        )

        games = log.GAME_ID.unique()

        for g in games:
            if len(str(g)) < 10:
                g = '00' + str(g)

            self.get_data({
                'GameID': g, 'Season': season, 'SeasonType': season_type
            })
