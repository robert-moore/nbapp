from scrappers import EndPoint, default_year


class GameLogs(EndPoint):
    base_url = 'http://stats.nba.com/stats/leaguegamelog'
    default_params = {
        'Counter': '1000',
        'DateFrom': '',
        'DateTo': '',
        'Direction': 'DESC',
        'LeagueID': '00',
        'PlayerOrTeam': 'P',
        'Season': default_year,
        'SeasonType': 'Regular Season',
        'Sorter': 'DATE'
    }


class PlayerAdvancedGameLogs(EndPoint):
    base_url = 'http://stats.nba.com/stats/playergamelogs'
    default_params = {
        'DateFrom': '',
        'DateTo': '',
        'GameSegment': '',
        'LastNGames': '0',
        'LeagueID': '00',
        'Location': '',
        'MeasureType': 'Base',
        'Month': '0',
        'OpponentTeamID': '0',
        'Outcome': '',
        'PORound': '0',
        'PaceAdjust': 'N',
        'PerMode': 'Totals',
        'Period': '0',
        'PlusMinus': 'N',
        'Rank': 'N',
        'Season': default_year,
        'SeasonSegment': '',
        'SeasonType': 'Regular Season',
        'ShotClockRange': '',
        'VsConference': '',
        'VsDivision': '',
    }


class TeamAdvancedGameLogs(EndPoint):
    base_url = 'http://stats.nba.com/stats/teamgamelogs'
    default_params = {
        'DateFrom': '',
        'DateTo': '',
        'GameSegment': '',
        'LastNGames': '0',
        'LeagueID': '00',
        'Location': '',
        'MeasureType': 'Base',
        'Month': '0',
        'OpponentTeamID': '0',
        'Outcome': '',
        'PORound': '0',
        'PaceAdjust': 'N',
        'PerMode': 'Totals',
        'Period': '0',
        'PlusMinus': 'N',
        'Rank': 'N',
        'Season': default_year,
        'SeasonSegment': '',
        'SeasonType': 'Regular Season',
        'ShotClockRange': '',
        'VsConference': '',
        'VsDivision': '',
    }