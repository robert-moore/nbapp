import hashlib
import logging
import pandas as pd
import requests
from dateutil.parser import parse as date_parse
from fuzzywuzzy import process
import sys

from util.data import raw_data_dir, file_check
from util.format import format_year_string, format_game_id

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

default_year = '2017-18'
request_headers = {
    'Host': 'stats.nba.com',
    'User-Agent': 'Mozilla/5.0',
    'Accept': 'application/json, text/plain, */*',
    'Accept-Language': 'en-US',
    'Accept-Encoding': 'gzip, deflate, br',
    # 'Cookie': 's_fid=5DCAD423B48A005D-0B2542CDA86AD52E; s_cc=true; s_sq=%5B%5BB%5D%5D',
    'Connection': 'keep-alive',
    'x-nba-stats-origin': 'stats',
    'x-nba-stats-token': 'true',
    'DNT': '1'
}
param_list = {
    'DateFrom': {'type': 'Date'},
    'DateTo': {'type': 'Date'},
    'MeasureType': {'type': 'Enum', 'choices': [
        'Base', 'Advanced', 'Misc', 'Scoring',
        'Usage', 'Opponent', 'Defense']},
    'SeasonType': {'type': 'Enum', 'choices': [
        'Pre Season', 'Regular Season', 'Playoffs', 'All Star']},
    'PerMode': {'type': 'Enum', 'choices': [
        'PerGame', 'Per100Possessions', 'PerPossession',
        'Per100Plays', 'PerPlay', 'PerMinute', 'Per36',
        'Per40', 'Per48', 'Totals']},
    'PlayerOrTeam': {'type': 'Enum', 'choices': ['Player', 'Team', 'P', 'T']},
    'PtMeasureType': {'type': 'Enum', 'choices': [
        'Drives', 'Defense', 'CatchShoot', 'Passing', 'Possessions',
        'PullUpShot', 'Rebounding', 'Efficiency', 'SpeedDistance',
        'ElbowTouch', 'PostTouch', 'PaintTouch']},
    'category': {'type': 'Enum',
                 'choices': ['Transition', 'Isolation', 'PRBallHandler', 'PRRollman', 'Postup', 'Spotup', 'Handoff',
                             'Cut', 'OffScreen', 'OffRebound']}
}


def validate_parameters(params, default_params):
    for key, value in params.items():
        if key not in default_params.keys():
            suggestions = process.extract(key, default_params.keys(), limit=3)
            raise ValueError('{} is not a valid value for this end point. Did you mean: {}?'.format(key, suggestions))
        if key in param_list:
            p = param_list[key]
            if p['type'] == 'Enum' and value not in p['choices']:
                suggestions = process.extract(value, p['choices'], limit=3)
                raise ValueError('{} is not a valid value for {}. Did you mean {}'.format(value, key, suggestions))
            if p['type'] == 'Date' and value != '':
                date_parse(value)


def construct_full_url(base, params):
    url = base + '?'
    url += ''.join(['{}='.format(param) + str(value).replace(' ', '+') + '&' for param, value in params.items()])
    return url


def json_to_pandas(json, index):
    data = json['resultSets'][index]
    headers = data['headers']
    rows = data['rowSet']
    data_dict = [dict(zip(headers, row)) for row in rows]
    return pd.DataFrame(data_dict)


class EndPoint:
    base_url = ''
    default_params = {}
    season_year_param = 'Season'
    index = 0

    def __init__(self):
        None

    def set_params(self, passed_params):
        validate_parameters(passed_params, self.default_params)
        params = self.default_params
        for key, value in passed_params.items():
            if key == 'GameID':
                params[key] = format_game_id(value)
            if key == 'Season':
                params[key] = format_year_string(value)
            else:
                params[key] = value
        return params

    def determine_file_path(self, params):
        param_string = ''
        for p in sorted(params):
            param_string += ',{}={}'.format(str(p), str(params[p]))
        param_hash = hashlib.sha1(param_string.encode('utf-8')).hexdigest()
        return raw_data_dir + self.base_url.split('/')[-1] + '/' + param_hash + '.csv'

    def get_data(self, passed_params=default_params, override_file=False):
        params = self.set_params(passed_params)
        file_path = self.determine_file_path(params)

        if (not file_check(file_path)) or override_file:
            full_url = construct_full_url(self.base_url, params)
            logging.info('API ==> {} {}'.format(self.base_url.split('/')[-1], str(passed_params)))
            r = requests.get(full_url, headers=request_headers)
            if r.status_code != 200:
                logging.error(r.reason + '@' + full_url)
                raise requests.ConnectionError(str(r.status_code) + ': ' + str(r.reason))

            df = json_to_pandas(r.json(), self.index)
            
            df.to_csv(file_path, encoding = 'utf-8')
            return pd.read_csv(file_path)
        else:
            logging.info('FILE => {} {}'.format(self.base_url.split('/')[-1], str(passed_params)))
            return pd.read_csv(file_path)

    def get_data_for_year_range(self, year_range, other_params):
        df = pd.DataFrame()
        for year in year_range:
            other_params[self.season_year_param] = year

            year_df = self.get_data(other_params)
            year_df['YEAR'] = year
            df = df.append(year_df)
        return df
