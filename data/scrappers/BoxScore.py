import pandas as pd

from scrappers.StatsEndpoint import EndPoint, default_year
from scrappers.GameLog import TeamAdvancedGameLogs
from util.data import raw_data_dir, file_check


class BoxScoreEndPoint(EndPoint):
    default_params = {
        'EndPeriod': '10',
        'EndRange': '28800',
        'GameID': '',
        'RangeType': '0',
        'Season': default_year,
        'SeasonType': 'Regular Season',
        'StartPeriod': '1',
        'StartRange': '0'
    }

    def set_index(self, index):
        self.index = index

    def determine_file_path(self, params):
        box_score_type = self.base_url.split('https://stats.nba.com/stats/boxscore')[1].split('v2')[0]
        return raw_data_dir + 'boxscore/' + params['GameID'] + '/' + box_score_type + str(self.index) + '.csv'


class BoxScoreSummary(BoxScoreEndPoint):
    base_url = 'https://stats.nba.com/stats/boxscoresummaryv2'
    default_params = {
        'GameID': ''
    }
    index = 1


class BoxScoreTraditional(BoxScoreEndPoint):
    base_url = 'https://stats.nba.com/stats/boxscoretraditionalv2'


class BoxScoreAdvanced(BoxScoreEndPoint):
    base_url = 'https://stats.nba.com/stats/boxscoreadvancedv2'


class BoxScoreMisc(BoxScoreEndPoint):
    base_url = 'https://stats.nba.com/stats/boxscoremiscv2'


class BoxScoreScoring(BoxScoreEndPoint):
    base_url = 'https://stats.nba.com/stats/boxscorescoringv2'


class BoxScoreUsage(BoxScoreEndPoint):
    base_url = 'https://stats.nba.com/stats/boxscoreusagev2'


class BoxScoreFourFactors(BoxScoreEndPoint):
    base_url = 'https://stats.nba.com/stats/boxscorefourfactorsv2'


class BoxScoreTracking(BoxScoreEndPoint):
    base_url = 'https://stats.nba.com/stats/boxscoreplayertrackv2'


class BoxScoreHustle(BoxScoreEndPoint):
    base_url = 'https://stats.nba.com/stats/hustlestatsboxscore'
    default_params = {
        'GameID': ''
    }

    def determine_file_path(self, params):
        return raw_data_dir + 'boxscore/' + params['GameID'] + '/' + 'hustle' + '.csv'
    index = 1


class BoxScoreMatchups(EndPoint):
    base_url = 'http://stats.nba.com/stats/boxscorematchups'
    default_params = {
        'GameID': ''
    }

    def determine_file_path(self, params):
        return raw_data_dir + 'boxscore/' + params['GameID'] + 'matchups.csv'

    def update_data(self, season=default_year, season_type='Regular Season'):
        log = TeamAdvancedGameLogs().get_data({'Season': season, 'SeasonType': season_type}, override_file=True)
        games = log.GAME_ID.unique()
        for g in games:
            self.get_data({'GameID': g})

    def aggregate_data(self, season=default_year, season_type='Regular Season',
                       override_file=False):

        file_path = raw_data_dir + '/boxscorematchups/aggregate_{}.csv'.format(season)

        if (not file_check(file_path)) or override_file:

            log = TeamAdvancedGameLogs().get_data({
                'Season': season, 'SeasonType': season_type},
                override_file=True
            )

            games = ['00' + str(g) if len(str(g)) < 10 else str(g)
                     for g in log.GAME_ID.unique()]

            season_df = pd.concat(
                [self.get_data({'GameID': g}) for g in games]
            )

            sum_col = [
                'AST', 'BLK', 'DEF_FOULS', 'FG3A', 'FG3M', 'FGA', 'FGM',
                'FTM', 'HELP_BLK', 'HELP_BLK_REC', 'OFF_FOULS', 'PLAYER_PTS',
                'POSS', 'SFL', 'TEAM_PTS', 'TOV'
            ]

            group_col = ['OFF_PLAYER_NAME', 'DEF_PLAYER_NAME']

            df = season_df.groupby(group_col)[sum_col].sum()
            df.reset_index(inplace=True)
            df = df[df['POSS'] >= 10]
            df.to_csv(file_path)
            return df
        else:
            return pd.read_csv(file_path)