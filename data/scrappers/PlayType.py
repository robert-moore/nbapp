import pandas as pd
import requests
import json

from scrappers.StatsEndpoint import EndPoint, validate_parameters, construct_full_url, param_list
from util.data import raw_data_dir, file_check


class SynergyEndpoint(EndPoint):
    default_params = {'category': 'Spotup',
                      'limit': '500',
                      'names': 'offensive',
                      'q': '2538084',
                      'season': '2017',
                      'seasonType': 'Reg'}

    synergy_request_headers = request_headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.5',
        'Connection': 'keep-alive',
        'DNT': '1',
        'Host': 'stats-prod.nba.com',
        'Origin': 'https://stats.nba.com',
        'Referer': 'https://stats.nba.com/players/spot-up/?sort=Percentile&dir=-1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0'
    }

    def determine_file_path(self, params):
        return raw_data_dir + 'play_type/' + self.base_url.split('/')[-2] + '/' + params['season'] + '/' + params[
            'seasonType'] + '/' + params['category'] + '/' + params['names'] + '.csv'

    def get_data(self, passed_params=default_params, override_file=False):
        validate_parameters(passed_params, self.default_params)
        params = self.set_params(passed_params)
        file_path = self.determine_file_path(params)

        if (not file_check(file_path)) or override_file:
            full_url = construct_full_url(self.base_url, params)
            print(full_url)
            r = requests.get(
                full_url,
                headers=self.synergy_request_headers
            )

            if r.status_code != 200:
                raise requests.ConnectionError(
                    '{}:{}'.format(r.status_code, r.reason)
                )
            
            if r.json():
                data = eval(json.dumps(r.json()['results']))
                df = pd.DataFrame(data)
                df.to_csv(file_path)
                return df
            else:
                return pd.DataFrame()

        else:
            print(file_path)
            return pd.read_csv(file_path)

    def get_data_for_all_play_types(self, season, override_file=False):
        all_df = pd.DataFrame(
            columns=['PlayerFirstName', 'PlayerLastName', 'TeamNameAbbreviation'])
        for pt in param_list['category']['choices']:
            params = self.default_params
            params['season'] = season
            params['category'] = pt

            pt_df = self.get_data(params, override_file)
            pt_df = pt_df[
                ['PlayerFirstName', 'PlayerLastName', 'TeamNameAbbreviation', 'Poss', 'Points', 'PPP', 'Time']]
            pt_df = pt_df.rename(columns={
                'Poss': '{}Poss'.format(pt),
                'Points': '{}Points'.format(pt),
                'PPP': '{}PPP'.format(pt),
                'Time': '{}Freq'.format(pt),
            })
            all_df = all_df.merge(pt_df, on=['PlayerFirstName', 'PlayerLastName', 'TeamNameAbbreviation'],
                                  how='outer')

        all_df = all_df.fillna(0)
        all_df['TotalPoss'] = 0
        all_df['TotalPoints'] = 0
        for pt in param_list['category']['choices']:
            all_df['TotalPoss'] += all_df['{}Poss'.format(pt)]
            all_df['TotalPoints'] += all_df['{}Points'.format(pt)]
        all_df['TotalPPP'] = all_df['TotalPoints'] / all_df['TotalPoss']

        return all_df.sort_values(by='TotalPoss', ascending=False)


class SynergyPlayerStats(SynergyEndpoint):
    base_url = 'https://stats-prod.nba.com/wp-json/statscms/v1/synergy/player/'


class SynergyTeamStats(SynergyEndpoint):
    base_url = 'https://stats-prod.nba.com/wp-json/statscms/v1/synergy/team/'
