from scrappers import GeneralPlayerStats
from util.format import format_year_string, format_game_id
from util.data import api_hist_data_dir

df = GeneralPlayerStats().get_data()[['TEAM_ID', 'TEAM_ABBREVIATION']]
df = df.rename(columns = {
    'TEAM_ID': 'id',
    'TEAM_ABBREVIATION': 'name'
})
df = df.drop_duplicates()
df = df.sort_values(by='id', ascending=True)

df.to_csv(api_hist_data_dir + 'teams.csv', index=False)
