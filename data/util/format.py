import sys
import re


def format_year_string(year):
    year = str(year)
    if re.match(r'[1-2][09][0-9]{2}-[0-9]{2}', year):
        return year
    elif re.match(r'[1-2][09][0-9]{2}', year):
        return str(year) + "-" + str(int(year) + 1)[2:4]
    else:
        raise ValueError('Unexpected Year Format!')


def format_game_id(game_id):
    game_id = str(game_id)
    while len(game_id) < 10:
        game_id = '0{}'.format(game_id)
    return game_id


def get_name_part(name):
    special_names = {
        'LeBron James': 'LeBron',
        'Draymond Green': 'Draymond',
        'Chris Paul': 'CP3',
        'Kemba Walker': 'Kemba',
        'DeAndre Jordan': 'DJ',
        'Isaiah Thomas': 'IT',
        'Gerald Wallace': 'G Wallace',
        'Taj Gibson': 'T. Gibson',
        'Terrence Jones': 'T Jones',
        'Wesley Matthew': 'W Matthews',
        'Jeff Green': 'J Green',
        'Brandon Jennings': 'B Jennings',
        'Giannis Antetokounmpo': 'Giannis',
        'Anthony Davis': 'AD',
        'Klay Thompson': 'Klay',
        'Lou Williams': 'Lou'
    }

    if len(name.split(',')) > 1:
        name = name.split(', ')[1] + ' ' + name.split(', ')[0]

    if name in special_names:
        return special_names[name]
    elif len(name.split(' ')) < 2:
        return name
    else:
        return name.split(' ')[1]


def get_display_name(name_col, year_col):
    year_part = year_col.apply(lambda x: '\'' + x.split('-')[1])
    name_part = name_col.apply(lambda x: get_name_part(x))
    return name_part + ' ' + year_part


def print_reddit_table(df, columns):
    for ix, col in enumerate(columns):
        try:
            df[col] = df[col].round(2)
        except TypeError:
            None
        sys.stdout.write(str(col) + (' | ' if ix is not len(columns) - 1 else ''))
    print('')
    for ix, col in enumerate(columns):
        sys.stdout.write(':--' + (' | ' if ix is not len(columns) - 1 else ''))
    print('')
    for ix, row in df.iterrows():
        for jx, col in enumerate(columns):
            try:
                sys.stdout.write(str(row[col]) + (' | ' if jx is not len(columns) - 1 else ''))
            except UnicodeEncodeError:
                sys.stdout.write(' ' + (' | ' if jx is not len(columns) - 1 else ''))
        print('')