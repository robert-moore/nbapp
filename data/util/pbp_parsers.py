def is_home(pbp_df, team_abb):
    pbp_df = pbp_df[pbp_df['PLAYER1_TEAM_ABBREVIATION'] == team_abb]
    home_df = pbp_df[pbp_df['VISITORDESCRIPTION'].isnull()]
    vis_df = pbp_df[pbp_df['HOMEDESCRIPTION'].isnull()]
    return len(home_df) > len(vis_df)


def get_team_df(pbp_df, team_abb):
    return pbp_df[pbp_df['PLAYER1_TEAM_ABBREVIATION'] == team_abb]


def convert_time(df):
    quarter = df['PERIOD']
    time = df['PCTIMESTRING']
    quarter.map(int)
    minutes = time.map(lambda x: x.split(':')[0]).map(int)
    seconds = time.map(lambda x: x.split(':')[1]).map(int)
    time_elapsed = (minutes * 60) + seconds
    previous_time = quarter.map(lambda x: ((x - 1) * 720) if x <= 4 else (2880 + ((x-5) * 300)))
    quarter_length = quarter.map(lambda x: 720 if x <= 4 else 300)
    return previous_time + (quarter_length - time_elapsed)


def merge_descriptions(df):
    df = df.fillna('')
    df['HOMEDESCRIPTION'] = df['HOMEDESCRIPTION'].map(str)
    df['VISITORDESCRIPTION'] = df['VISITORDESCRIPTION'].map(str)
    df['DESC'] = df[['HOMEDESCRIPTION', 'VISITORDESCRIPTION']].apply(lambda x: ''.join(x), axis=1)
    return df['DESC']
