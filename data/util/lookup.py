from scrappers.GameLog import TeamAdvancedGameLogs
from util.format import format_year_string, format_game_id


def get_info_for_game(game_id):
    game_id = format_game_id(game_id)
    first_year_ending = game_id[3:5]

    season_type_map = {
        '1': 'Pre Season',
        '2': 'Regular Season',
        '4': 'Playoffs'
    }

    season_type = season_type_map[game_id[2]]

    if int(first_year_ending) > 90:
        season = format_year_string('19' + first_year_ending)
    else:
        season = format_year_string('20' + first_year_ending)

    log = TeamAdvancedGameLogs().get_data({'Season': season, 'SeasonType': season_type})
    game_date = log[log['GAME_ID'] == int(game_id)].iloc[0]['GAME_DATE'].split('T')[0]

    return {
        'date': game_date,
        'season': str(season),
        'season_type': str(season_type)
    }
