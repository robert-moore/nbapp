from util.format import format_year_string, format_game_id
from util.lookup import get_info_for_game
from util.data import api_data_dir
from helpers.stats import get_box_score_player_stats, get_tracking_stats_for_date, get_stats_from_pbp
from scrappers.GameLog import TeamAdvancedGameLogs
from scrappers.PlayByPlay import PlayByPlay
import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.ERROR)


def get_stats_for_game(season, season_type, game_id, override_tracking):
    game_info = get_info_for_game(format_game_id(game_id))

    pbp_ep = PlayByPlay()

    pbp_df = pbp_ep.get_data({
        'GameID': game_id, 
        'Season': season, 
        'SeasonType': season_type
    })

    df = get_box_score_player_stats(format_game_id(game_id), season_type)
    
    tracking = get_tracking_stats_for_date(game_info['date'], season, season_type, data_override=override_tracking)
    df = df.merge(tracking, on='PLAYER_NAME', how='left')

    scoring = get_stats_from_pbp(pbp_df, format_game_id(game_id), season, season_type)
    df = df.merge(scoring, on='PLAYER_NAME', how='outer')

    # df = df.append(df, sort=False)

    columns = {
        'GAME_ID': 'game_id',
        'PLAYER_NAME': 'player',
        'TEAM_ABBREVIATION': 'team',
        'MIN': 'time',
        'START_POSITION': 'starter',
        '2PT_FGA': 'fg2a',
        '2PT_FGM': 'fg2m',
        'FG3A': 'fg3a',
        'FG3M': 'fg3m',
        'AND_ONE': 'ft1a',
        '2PT_FTA': 'ft2a',
        '3PT_FTA': 'ft3a',
        'TECH_FTA': 'tech_fta',
        'FTM': 'ftm',
        'AST': 'ast',
        'POTENTIAL_AST': 'pot_ast',
        'FTAST': 'ft_ast',
        'SAST': 'sec_ast',
        'PASS': 'passes',
        'AST_PTS': 'ast_pts',
        'TO': 'tov',
        'OREB': 'oreb',
        'DREB': 'dreb',
        'ORBC': 'orebc',
        'DRBC': 'drebc',
        'BOX_OUTS': 'box_outs',
        'BLK': 'blk',
        'DFGA': 'dfga_rim',
        'DFGM': 'dfgm_rim',
        'STL': 'stl',
        'DEFLECTIONS': 'defl',
        'LOOSE_BALLS_RECOVERED': 'recoveries',
        'CHARGES_DRAWN': 'charges_drawn',
        'CONTESTED_SHOTS_2PT': 'cont_fg2a',
        'CONTESTED_SHOTS_3PT': 'cont_fg3a',
        'SCREEN_ASSISTS': 'screen_ast',
        'TCHS': 'touch',
        'ELBOW_TOUCHES': 'elbow_touch',
        'POST_TOUCHES': 'post_touch',
        'PAINT_TOUCHES': 'paint_touch',
        'SPD': 'speed',
        'PLUS_MINUS': 'plus_minus',
        'OFF_RATING': 'ortg',
        'DEF_RATING': 'drtg',
        'PACE': 'pace',
        'TIME_OF_POSS': 'poss_time'
    }

    df = df.rename(columns=columns)

    df = df[columns.values()]

    df = df.fillna(0)

    df['time'] = df['time'].apply(lambda x: (
        int(x.split(':')[0]) * 60) + int(x.split(':')[1]) if type(x) is str else 0)
    df['starter'] = df['starter'].apply(lambda x: x != 0)
    df['distance'] = df['time'] * df['speed']
    df['game_id'] = df['game_id'].apply(lambda x: format_game_id(x))

    return df
