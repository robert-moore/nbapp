import pandas as pd

from scrappers.Players import GeneralPlayerStats
from config import all_seasons, all_season_types
from util.format import format_year_string, format_game_id
from util.data import api_hist_data_dir


players_ep = GeneralPlayerStats()
data_df = pd.DataFrame()

for season in all_seasons:
    season_string = format_year_string(season)

    for season_type in all_season_types:
        end_point_df = players_ep.get_data({'Season': season_string, 'SeasonType': season_type})
        if len(end_point_df) > 0:
            end_point_df = end_point_df[['PLAYER_NAME', 'PLAYER_ID']]
            end_point_df = end_point_df.rename(columns = {
                'PLAYER_NAME': 'name',
                'PLAYER_ID': 'id'
            })
            data_df = data_df.append(end_point_df)

data_df = data_df.drop_duplicates()
data_df.to_csv(api_hist_data_dir + 'players.csv', index=False)
