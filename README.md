# App

The app is not currently being hosted publicly. Closer to the season I plan to have it hosted at [nbaviz.ninja](http://www.nbaviz.ninja).

The home page is a game table:

![home page](https://i.imgur.com/GcQdZcG.png)

Clicking the details button on any row will take you to a page with the rotations vizualization, shots charts, and stats for that game:

![game page](https://i.imgur.com/BeFwtno.png)

At the top right are navigation buttons that will take you back to the games page, to the teams home page, or scroll to the bottom with information on me, and a link to this repo

Clicking the teams button will take you to a selection for teams:

![team page](https://i.imgur.com/oWeV3c2.png)

Selecting a team will take you to that team's page for the selected filters at the top. You can also click on any team logo from the details page for a game to see that team's page.

![team details page](https://i.imgur.com/C1vjLCU.png)

# Technology Stack

Postgres DB

Python Flask API

Angular Web App

D3.js

# Docker

To run the app with docker run `docker-compose up`

A reverse proxy is in place to route requests to api. The app can be accessed on port 80

# Running Without Docker

For development, it can be easier to run without docker as it will be faster to see the latest changes take place.

## Postgres DB

The Python Flask API will require a local postgres database named `flask_db`

## Python Flask API

In the `api` directory:

The required python modules are listed in the `requirements.txt`. To install with pip run `pip install -r requirements.txt`.

To add the necessary schema to your database run `flask db upgrade`.

To add data to the database run `python buildDB.py`. Currently this will add all games from the 2014-15 to 2017-18 regular seasons and playoffs to the DB.

Once the requirements are installed and the database is set up, run `python app.py` to run the flask app, which will run on `localhost:5000`.

There are two api endpoints to get data which return a JSON response:

`api/games` will return all games with teams, scores, dates, and the game id from stats.nba.com.

`api/gameDetail/<game_id>` will return the same information returned by the `/games` endpoint for the specified game and also player rotation, shots, and score margin information.

`api/teams` will return all teams

`api/teamDetail` will return the required data for the team detail page. This end point requires post paramaters


## Angular Web App

In the `web` directory:

Run `npm install` to install the required node modules

Run `npm install @angular\cli` to install the angular cli. With that you can run `ng serve` to run the web app. The web app will run on port 4200. Navigate to `localhost:4200` to see the web app in your browser.

# Kubernetes

The deployment and service yaml files for Kubernetes container management are located in the `kubernetes` directory along with some convenience scripts to update the deployments. There is also a secrets file for creating passwords for the database and api. In the code repo they are set to a series of x's. I've changed them for my actual deployment.

To use kubernetes you will need the google cloud sdk (https://cloud.google.com/sdk/docs/quickstarts) and the kubernetes command line tool, kubectl, (https://kubernetes.io/docs/tasks/tools/install-kubectl/). Once install you will also need a project in the Google Kubernetes Engine (https://cloud.google.com/kubernetes-engine/).

Once that is setup set your default project id and compute zone

`gcloud config set project PROJECT_ID`

`gcloud config set compute/zone us-central1-b`

Now you can create the necessary secret, deployments, and services.

In my repo the kubernetes deployments are pulling the docker images from this gitlab registry.
